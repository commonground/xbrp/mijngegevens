import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  Preferences(this._preferences);

  final SharedPreferences _preferences;
  final String hasSeenIntroductionKey = 'hasSeenIntroduction';
  final String isLoggedInKey = 'isLoggedIn';

  bool getHasSeenIntroduction() => _preferences.getBool(hasSeenIntroductionKey) ?? false;
  void setHasSeenIntroduction() => _preferences.setBool(hasSeenIntroductionKey, true);

  bool getIsLoggedIn() => _preferences.getBool(isLoggedInKey) ?? false;
  void setIsLoggedIn() => _preferences.setBool(isLoggedInKey, true);
}
