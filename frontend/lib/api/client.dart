import 'dart:math';

import 'package:flutter/services.dart';
import 'package:mijngegevensapp/model/address.dart';
import 'package:mijngegevensapp/model/data_request.dart';
import 'package:mijngegevensapp/model/data_usage.dart';
import 'package:mijngegevensapp/model/history.dart';
import 'package:mijngegevensapp/model/history_entry.dart';
import 'package:mijngegevensapp/model/personal_data.dart';
import 'package:mijngegevensapp/model/profile_picture.dart';
import 'package:mijngegevensapp/model/property.dart';
import 'package:mijngegevensapp/model/report_error_request.dart';

class Client {
  Future _randomTimeout() => Future.delayed(Duration(milliseconds: (Random().nextDouble() * 1000).toInt()));
  String _randomEventID() => '#${Random().nextInt(99999999).toString()}';

  Future<T> get<T>() async {
    if (T == PersonalData) {
      return await _getPersonalData() as T;
    }
    if (T == ProfilePicture) {
      return await _getProfilePicture() as T;
    }
    if (T == DataUsage) {
      return await _getDataUsage() as T;
    }
    if (T == History) {
      return await _getHistory() as T;
    }
    throw Exception('Unknown request: ${T.runtimeType}');
  }

  Future<B> send<T, B>(final T data) async {
    if (T == ReportErrorRequest) {
      return await _sendReportError(data as ReportErrorRequest) as B;
    }
    throw Exception('Unknown request: ${T.runtimeType}');
  }

  Future<bool> _sendReportError(final ReportErrorRequest request) async {
    await _randomTimeout();
    return true;
  }

  Future<PersonalData> _getPersonalData() async {
    await _randomTimeout();
    return PersonalData(
      [
        Property<PersonalDataType>(PropertyKey.personalDataType, PersonalDataType.self),
        Property<String>(
          PropertyKey.firstName,
          'Jill Johanna',
          effectiveDate: DateTime(1994, 6, 21),
          inclusionDate: DateTime(1994, 6, 22),
          registeredBy: 'Gemeente Oudescha',
        ),
        Property<String>(PropertyKey.lastName, 'Bloem'),
        Property<UsedNameType>(PropertyKey.usedNameType, UsedNameType.ownName),
        Property<String>(PropertyKey.bsn, '123456789'),
        Property<DateTime>(PropertyKey.birthDate, DateTime(1994, 6, 21)),
        Property<String>(PropertyKey.birthPlace, 'Oudescha'),
        Property<String>(PropertyKey.birthCountry, 'Nederland'),
        Property<bool>(PropertyKey.dutchNationality, true),
        Property<Address>(
          PropertyKey.residentialAddress,
          Address(
            streetName: 'Kelvinlaan',
            houseNumber: '9',
            postalCode: '4502 GH Groenestede',
          ),
          effectiveDate: DateTime(1994, 6, 21),
          inclusionDate: DateTime(1994, 6, 22),
          registeredBy: 'Oudescha',
        ),
        Property<String>(PropertyKey.municipality, 'Groenestede'),
        Property<List<PersonalData>>(
          PropertyKey.relatives,
          [
            PersonalData(
              [
                Property<PersonalDataType>(PropertyKey.personalDataType, PersonalDataType.parent),
                Property<String>(PropertyKey.firstName, 'Marie'),
                Property<String>(PropertyKey.lastName, 'Hoekstra'),
                Property<String>(PropertyKey.municipality, 'Groenestede'),
                Property<String>(
                  PropertyKey.parent,
                  'Marie Hoekstra, geboren 08-08-1968, Achterscha, Nederland',
                ),
                Property<DateTime>(
                  PropertyKey.effectiveDateParentChildRelationship,
                  DateTime(1994, 6, 21),
                ),
              ],
            ),
            PersonalData(
              [
                Property<PersonalDataType>(PropertyKey.personalDataType, PersonalDataType.parent),
                Property<String>(PropertyKey.firstName, 'Jan'),
                Property<String>(PropertyKey.lastName, 'Bloem'),
                Property<String>(PropertyKey.municipality, 'Groenestede'),
                Property<String>(
                  PropertyKey.parent,
                  'Jan Bloem, geboren 03-03-1963, Oudescha, Nederland',
                ),
                Property<DateTime>(
                  PropertyKey.effectiveDateParentChildRelationship,
                  DateTime(1994, 6, 21),
                ),
              ],
            ),
            PersonalData(
              [
                Property<PersonalDataType>(PropertyKey.personalDataType, PersonalDataType.relationship),
                Property<String>(PropertyKey.firstName, 'Pim'),
                Property<String>(PropertyKey.lastName, 'van Hierden'),
                Property<String>(PropertyKey.municipality, 'Groenestede'),
                Property<String>(
                  PropertyKey.marriedTo,
                  'Pim van Hierden, geboren 14-01-1989, Buitenstad, Anderland',
                ),
                Property<DateTime>(
                  PropertyKey.marriageDate,
                  DateTime(2021, 6, 6),
                ),
                Property<String>(
                  PropertyKey.marriagePlace,
                  'Heidedal',
                ),
                Property<String>(
                  PropertyKey.marriageCountry,
                  'Nederland',
                ),
              ],
            ),
            PersonalData(
              [
                Property<PersonalDataType>(PropertyKey.personalDataType, PersonalDataType.child),
                Property<String>(PropertyKey.firstName, 'Dango'),
                Property<String>(PropertyKey.lastName, 'van Hierden-Bloem'),
                Property<String>(PropertyKey.municipality, 'Groenestede'),
                Property<String>(
                  PropertyKey.child,
                  'Dango van Hierden-Bloem, geboren 18-07-2020, Oudescha, Nederland',
                ),
                Property<DateTime>(
                  PropertyKey.effectiveDateParentChildRelationship,
                  DateTime(2020, 7, 18),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Future<ProfilePicture> _getProfilePicture() async {
    await _randomTimeout();
    return ProfilePicture((await rootBundle.load('assets/jill_bloem.png')).buffer.asUint8List());
  }

  Future<DataUsage> _getDataUsage() async {
    await _randomTimeout();
    return DataUsage(
      requests: [
        DataRequest(
          organization: 'RWD',
          date: DateTime(2021, 8, 14),
          reasonOfUse: 'Besluit Tenaamstelling kenteken voertuig',
          reason: 'RDW ontvangt deze gegevens omdat een voertuig op uw naam komt te staan.',
          accessedInformation: {
            InformationType.basicInformation,
            InformationType.residentialAddress,
          },
        ),
        DataRequest(
          organization: 'SVB',
          date: DateTime(2021, 7, 24),
          reason: 'Besluit Kinderbijslag',
          reasonOfUse: 'Besluit Kinderbijslag',
          accessedInformation: {
            InformationType.basicInformation,
            InformationType.residentialAddress,
          },
        ),
      ],
    );
  }

  Future<History> _getHistory() async {
    await _randomTimeout();
    return History(
      entries: [
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Wijziging naamgebruik geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.info),
            Property<DateTime>(PropertyKey.date, DateTime(2021, 10, 7)),
            Property<String>(PropertyKey.municipality, 'Groenestede'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Huwelijk geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.relationship),
            Property<DateTime>(PropertyKey.date, DateTime(2021, 6, 6, 11, 10)),
            Property<String>(PropertyKey.municipality, 'Heidedal'),
            Property<String>(PropertyKey.partner1, 'Jill Johanna Bloem'),
            Property<String>(PropertyKey.partner2, 'Pim van Hierden'),
            Property<DateTime>(PropertyKey.weddingDate, DateTime(2021, 6, 6)),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Woonadres gecorrigeerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.residence),
            Property<DateTime>(PropertyKey.date, DateTime(2020, 12, 17)),
            Property<String>(PropertyKey.municipality, 'Groenestede'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Verhuizing in NL geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.residence),
            Property<DateTime>(PropertyKey.date, DateTime(2021, 12, 17)),
            Property<String>(PropertyKey.municipality, 'Groenestede'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Geboorte geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.birth),
            Property<DateTime>(PropertyKey.date, DateTime(2020, 7, 18)),
            Property<String>(PropertyKey.municipality, 'Oudescha'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Verhuizing in NL geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.residence),
            Property<DateTime>(PropertyKey.date, DateTime(2016, 11)),
            Property<String>(PropertyKey.municipality, 'Groenestede'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'BSN toegekend'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.bsn),
            Property<DateTime>(PropertyKey.date, DateTime(1994, 6, 22)),
            Property<String>(PropertyKey.municipality, 'Oudescha'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Woonadres NL geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.residence),
            Property<DateTime>(PropertyKey.date, DateTime(1994, 6, 22)),
            Property<String>(PropertyKey.municipality, 'Oudescha'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Persoon ingeschreven'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.person),
            Property<DateTime>(PropertyKey.date, DateTime(1994, 6, 22)),
            Property<String>(PropertyKey.municipality, 'Oudescha'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Namen geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.name),
            Property<DateTime>(PropertyKey.date, DateTime(1994, 6, 22)),
            Property<String>(PropertyKey.municipality, 'Oudescha'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Nationaliteitsgegevens geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.nationality),
            Property<DateTime>(PropertyKey.date, DateTime(1994, 6, 22)),
            Property<String>(PropertyKey.municipality, 'Oudescha'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Ouderschap geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.parent),
            Property<DateTime>(PropertyKey.date, DateTime(1994, 6, 22)),
            Property<String>(PropertyKey.municipality, 'Oudescha'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Geslachtsaanduiding geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.sexDesignation),
            Property<DateTime>(PropertyKey.date, DateTime(1994, 6, 22)),
            Property<String>(PropertyKey.municipality, 'Oudescha'),
          ],
        ),
        HistoryEntry(
          [
            Property<String>(PropertyKey.eventID, _randomEventID()),
            Property<String>(PropertyKey.title, 'Geboorte geregistreerd'),
            Property<HistoryCategory>(PropertyKey.category, HistoryCategory.birth),
            Property<DateTime>(PropertyKey.date, DateTime(1994, 6, 22)),
            Property<String>(PropertyKey.municipality, 'Oudescha'),
          ],
        ),
      ],
    );
  }
}
