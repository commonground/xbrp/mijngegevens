import 'package:flutter/material.dart';

const appHeaderColor = Color(0xFF113450);
const iconColor = Color(0xFF0D71BC);
const primaryButtonColor = Color(0xFF1C5988);
const secondaryButtonColor = Color(0xFFB5DDFB);
const backgroundColor = Color(0xFFE0DED8);
const categoryColor = Color(0xFFD8E3EB);
const textAndValueColor = Color(0xFF333333);
const valueLabelColor = Color(0xFF828282);
const whiteTextColor = Color(0xFFFFFFFF);

ThemeData mijngegevensAppTheme(final BuildContext context) => ThemeData(
      appBarTheme: const AppBarTheme(
        color: appHeaderColor,
      ),
      iconTheme: const IconThemeData(
        color: iconColor,
      ),
      cardTheme: const CardTheme(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
      ),
      textTheme: Theme.of(context).textTheme.apply(
            bodyColor: textAndValueColor,
          ),
      snackBarTheme: const SnackBarThemeData(
        backgroundColor: Colors.black,
        behavior: SnackBarBehavior.floating,
      ),
    );
