import 'package:intl/intl.dart';

final _dateFormat = DateFormat('dd-MM-y');
final _dateTimeFormat = DateFormat('dd-MM-y H:m');

String dateFormat(final DateTime date) => _dateFormat.format(date);

String dateTimeFormat(final DateTime date) => _dateTimeFormat.format(date);

String smartFormat(final DateTime date) {
  if (date.isAtSameMomentAs(DateTime(date.year, date.month, date.day))) {
    return dateFormat(date);
  } else {
    return dateTimeFormat(date);
  }
}

extension Capitalization on String {
  String get uncapitalized => this[0].toLowerCase() + substring(1);
}
