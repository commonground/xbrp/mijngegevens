import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:mijngegevensapp/api/client.dart';
import 'package:mijngegevensapp/bloc/report_error_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/model/property.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/page_wrapper.dart';
import 'package:mijngegevensapp/widgets/primary_button.dart';
import 'package:provider/provider.dart';
import 'package:rx_bloc/rx_bloc.dart';

class ReportErrorPage extends StatefulWidget {
  const ReportErrorPage({
    required final this.title,
    required final this.properties,
    required final this.municipality,
    final Key? key,
  }) : super(key: key);

  final String title;
  final List<Property> properties;
  final String municipality;

  @override
  State<ReportErrorPage> createState() => _ReportErrorPageState();
}

class _ReportErrorPageState extends State<ReportErrorPage> {
  final _textController = TextEditingController();
  final _scrollController = ScrollController();
  final _node = FocusNode();
  final _formKey = GlobalKey<FormState>();
  late ReportErrorBloc _reportErrorBloc;

  @override
  void initState() {
    super.initState();
    _reportErrorBloc = ReportErrorBloc(
      Provider.of<Client>(
        context,
        listen: false,
      ),
    );
    _node.addListener(() async {
      if (_node.hasFocus) {
        await _scrollDown();
      }
    });
    _reportErrorBloc.sent.listen((final event) {
      if (event is ResultSuccess<bool> && event.data) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context).errorWillBeProcessed(widget.municipality)),
          ),
        );
        // Close report error page
        Navigator.of(context).pop();
        // Close property details page and return error reported date
        Navigator.of(context).pop(DateTime.now());
      }
    });
  }

  Future _scrollDown() async {
    await Future.delayed(const Duration(milliseconds: 300));
    await _scrollController.animateTo(
      _scrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 500),
      curve: Curves.ease,
    );
  }

  @override
  Widget build(final BuildContext context) => PageWrapper(
        sheetPage: true,
        title: AppLocalizations.of(context).reportError,
        body: Form(
          key: _formKey,
          child: Expanded(
            child: ListView(
              controller: _scrollController,
              padding: const EdgeInsets.all(20),
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(5)),
                    border: Border.all(
                      color: iconColor,
                      width: 2,
                    ),
                  ),
                  padding: const EdgeInsets.all(15),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Icon(Icons.error),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context).reportErrorInstructions(widget.municipality), // TODO
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Text(
                  AppLocalizations.of(context).errorIn(widget.title),
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  decoration: const BoxDecoration(
                    color: backgroundColor,
                    borderRadius: BorderRadius.all(Radius.circular(3)),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      for (final property in widget.properties) ...[
                        Text(
                          property.getDisplayValue(context),
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                      ],
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  AppLocalizations.of(context).yourMessage,
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                TextFormField(
                  controller: _textController,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  minLines: 3,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                  focusNode: _node,
                  validator: (final value) {
                    if (value == null || value.isEmpty) {
                      return '';
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                PrimaryButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      _reportErrorBloc.send(widget.properties, _textController.text);
                      await _scrollDown();
                    }
                  },
                  child: Text(AppLocalizations.of(context).sendMessage),
                ),
                RxResultBuilder<ReportErrorBloc, bool>(
                  bloc: _reportErrorBloc,
                  state: (final bloc) => bloc.sent,
                  buildSuccess: (final context, final sent, final bloc) => Container(),
                  buildError: (final context, final sent, final bloc) => const Icon(Icons.error),
                  buildLoading: (final context, final bloc) => Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: const Align(
                      widthFactor: 1,
                      heightFactor: 1,
                      child: CircularProgressIndicator(
                        color: iconColor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
