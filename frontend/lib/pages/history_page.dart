import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:intersperse/intersperse.dart';
import 'package:mijngegevensapp/bloc/history_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/model/history.dart';
import 'package:mijngegevensapp/model/history_entry.dart';
import 'package:mijngegevensapp/pages/property_details_page.dart';
import 'package:mijngegevensapp/pages/report_error_page.dart';
import 'package:mijngegevensapp/utils/formats.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/loading_or_link_indicator.dart';
import 'package:mijngegevensapp/widgets/page_wrapper.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

class HistoryPage extends StatelessWidget {
  const HistoryPage({
    final Key? key,
  }) : super(key: key);

  @override
  Widget build(final BuildContext context) => PageWrapper(
        title: AppLocalizations.of(context).history,
        body: RxResultBuilder<HistoryBloc, History>(
          state: (final bloc) => bloc.data as BehaviorSubject<Result<History>>,
          buildSuccess: (final context, final history, final bloc) {
            final padWidth = (log(history.entries.length) / log(10)).floorToDouble().toInt() + 1;
            return ListView(
              padding: const EdgeInsets.symmetric(vertical: 10),
              children: <Widget>[
                for (final entry in history.entries) ...[
                  GestureDetector(
                    onTap: entry.hasRelevantProperties
                        ? () {
                            Navigator.of(context).push<DateTime>(
                              MaterialPageRoute(
                                builder: (final context) => PropertiesDetailsPage(
                                  properties: entry.relevantProperties,
                                  title: entry.title.value,
                                  icon: entry.icon,
                                  onErrorPage: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (final _) => ReportErrorPage(
                                          title: entry.title.value,
                                          properties: entry.relevantProperties,
                                          municipality: entry.municipality.value,
                                        ),
                                      ),
                                    );
                                  },
                                  registeredBy: entry.municipality.value,
                                  inclusionDate: entry.date.value,
                                  eventID: entry.eventID.value,
                                ),
                              ),
                            );
                          }
                        : null,
                    child: Container(
                      padding: const EdgeInsets.only(
                        top: 5,
                        bottom: 5,
                        left: 15,
                        right: 15,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(entry.icon),
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.symmetric(horizontal: 15),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    entry.title.value,
                                    style: const TextStyle(
                                      fontSize: 16,
                                    ),
                                  ),
                                  Text(
                                    AppLocalizations.of(context).historyEntryDateMunicipality(
                                      dateFormat(entry.date.value),
                                      entry.municipality.value,
                                    ),
                                    style: const TextStyle(
                                      color: valueLabelColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Text(
                            (history.entries.length - history.entries.indexOf(entry)).toString().padLeft(padWidth, '0'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ]
                  .intersperse(
                    const Divider(
                      color: backgroundColor,
                    ),
                  )
                  .toList(),
            );
          },
          buildError: (final context, final error, final bloc) => const Icon(Icons.error),
          buildLoading: (final context, final bloc) => Container(
            padding: const EdgeInsets.all(10),
            child: const LoadingOrLinkIndicator(),
          ),
        ),
      );
}
