import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:intersperse/intersperse.dart';
import 'package:mijngegevensapp/bloc/data_usage_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/model/data_usage.dart';
import 'package:mijngegevensapp/pages/data_usage_details_page.dart';
import 'package:mijngegevensapp/utils/formats.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/information_type_box.dart';
import 'package:mijngegevensapp/widgets/loading_or_link_indicator.dart';
import 'package:mijngegevensapp/widgets/page_wrapper.dart';
import 'package:mijngegevensapp/widgets/value_field.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

class DataUsagePage extends StatelessWidget {
  const DataUsagePage({
    final Key? key,
  }) : super(key: key);

  double get _dotSize => 24;
  double get _borderThickness => 1;

  @override
  Widget build(final BuildContext context) => PageWrapper(
        title: AppLocalizations.of(context).whoUsesMyData,
        body: ListView(
          children: [
            Container(
              padding: const EdgeInsets.all(20),
              child: Text(
                AppLocalizations.of(context).dataUsageInfo,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 16,
                ),
              ),
            ),
            Container(
              color: backgroundColor,
              padding: const EdgeInsets.all(20),
              child: RxResultBuilder<DataUsageBloc, DataUsage>(
                state: (final bloc) => bloc.data as BehaviorSubject<Result<DataUsage>>,
                buildSuccess: (final context, final dataUsage, final bloc) {
                  final sortedDates = dataUsage.requests.map((final r) => r.date).toSet().toList()
                    ..sort((final a, final b) => b.millisecondsSinceEpoch.compareTo(a.millisecondsSinceEpoch));
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      for (final date in sortedDates) ...[
                        Row(
                          children: [
                            ClipOval(
                              child: Container(
                                height: _dotSize,
                                width: _dotSize,
                                color: iconColor,
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(left: 5),
                              child: Text(
                                dateFormat(date),
                                style: Theme.of(context).textTheme.subtitle1!.copyWith(color: appHeaderColor),
                              ),
                            ),
                          ],
                        ),
                        for (final request in dataUsage.requests.where((final r) => r.date == date)) ...[
                          Container(
                            margin: EdgeInsets.only(left: (_dotSize - _borderThickness) / 2),
                            padding: EdgeInsets.only(left: (_dotSize - _borderThickness) / 2, top: 10, bottom: 10),
                            decoration: BoxDecoration(
                              border: Border(
                                left: BorderSide(
                                  color: iconColor,
                                  width: _borderThickness,
                                ),
                              ),
                            ),
                            child: _buildCard(
                              context: context,
                              title: request.organization,
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (final context) => DataUsageDetailsPage(
                                      request: request,
                                    ),
                                  ),
                                );
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ValueField(
                                    label: AppLocalizations.of(context).dataUsed,
                                    value: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        for (final info in request.accessedInformation) ...[
                                          SizedBox(
                                            // TODO: this is not nice, but making it fill the width is somehow not possible
                                            width: MediaQuery.of(context).size.width - 92,
                                            child: InformationTypeBox(
                                              info: info,
                                            ),
                                          ),
                                        ],
                                      ],
                                    ),
                                  ),
                                  ValueField(
                                    label: AppLocalizations.of(context).reasonOfUse,
                                    value: request.reasonOfUse,
                                  ),
                                ]
                                    .intersperse(
                                      Container(
                                        height: 20,
                                      ),
                                    )
                                    .toList(),
                              ),
                            ),
                          )
                        ],
                      ],
                    ],
                  );
                },
                buildError: (final context, final error, final bloc) => const Icon(Icons.error),
                buildLoading: (final context, final bloc) => const LoadingOrLinkIndicator(),
              ),
            ),
          ],
        ),
      );

  Widget _buildCard({
    required final BuildContext context,
    required final String title,
    required final Widget child,
    final Function()? onTap,
  }) =>
      GestureDetector(
        onTap: onTap,
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      title,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                    if (onTap != null) ...[
                      const Icon(
                        Icons.arrow_right_sharp,
                        size: 28,
                      ),
                    ] else ...[
                      const SizedBox(
                        height: 28,
                      ),
                    ],
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                child: child,
              ),
            ],
          ),
        ),
      );
}
