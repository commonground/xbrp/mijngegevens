import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:mijngegevensapp/bloc/data_usage_bloc.dart';
import 'package:mijngegevensapp/bloc/history_bloc.dart';
import 'package:mijngegevensapp/bloc/introduction_bloc.dart';
import 'package:mijngegevensapp/bloc/personal_data_bloc.dart';
import 'package:mijngegevensapp/bloc/profile_picture_bloc.dart';
import 'package:mijngegevensapp/bloc/session_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/model/data_usage.dart';
import 'package:mijngegevensapp/model/personal_data.dart';
import 'package:mijngegevensapp/pages/data_usage_page.dart';
import 'package:mijngegevensapp/pages/introduction_page.dart';
import 'package:mijngegevensapp/pages/personal_data_page.dart';
import 'package:mijngegevensapp/utils/formats.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/loading_or_link_indicator.dart';
import 'package:mijngegevensapp/widgets/page_wrapper.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

class HomePage extends StatefulWidget {
  const HomePage({final Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late IntroductionBloc _introductionBloc;
  late SessionBloc _sessionBloc;
  late PersonalDataBloc _personalDataBloc;
  late DataUsageBloc _dataUsageBloc;
  late ProfilePictureBloc _profilePictureBloc;
  late HistoryBloc _historyBloc;

  @override
  void initState() {
    super.initState();
    _introductionBloc = RxBlocProvider.of<IntroductionBloc>(context);
    _sessionBloc = RxBlocProvider.of<SessionBloc>(context);
    _personalDataBloc = RxBlocProvider.of<PersonalDataBloc>(context);
    _dataUsageBloc = RxBlocProvider.of<DataUsageBloc>(context);
    _profilePictureBloc = RxBlocProvider.of<ProfilePictureBloc>(context);
    _historyBloc = RxBlocProvider.of<HistoryBloc>(context);
    // This is stupid, but otherwise the BloC won't load
    _historyBloc.data.listen((final _) {});
    _introductionBloc.hasSeen.listen((final seen) {
      WidgetsBinding.instance!.addPostFrameCallback((final _) {
        if (!seen) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (final context) => IntroductionPage(),
            ),
          );
          return;
        }
        _sessionBloc.checkIsLoggedIn();
      });
    });
    _sessionBloc.isLoggedIn.listen((final loggedIn) {
      if (!loggedIn) {
        return;
      }
      WidgetsBinding.instance!.addPostFrameCallback((final _) {
        _personalDataBloc.load();
        _dataUsageBloc.load();
        _profilePictureBloc.load();
        _historyBloc.load();
      });
    });
    _introductionBloc.checkHasSeen();
  }

  @override
  Widget build(final BuildContext context) => PageWrapper(
        showDrawer: true,
        title: AppLocalizations.of(context).title,
        body: Container(
          color: backgroundColor,
          child: Stack(
            children: [
              Opacity(
                opacity: 0.2,
                child: ClipRect(
                  child: Align(
                    alignment: Alignment.topCenter,
                    heightFactor: 0.6,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      heightFactor: 0.6,
                      child: Image.asset('assets/home_background.jpg'),
                    ),
                  ),
                ),
              ),
              RefreshIndicator(
                color: iconColor,
                onRefresh: () {
                  if (!_sessionBloc.isLoggedIn.value) {
                    return Future.value();
                  }
                  final completer = Completer();

                  _personalDataBloc.load();
                  _dataUsageBloc.load();

                  int finished = 0;
                  List<StreamSubscription<bool>> subscriptions = [];

                  void finish(final bool reloading) {
                    if (!reloading) {
                      finished++;
                      if (finished == subscriptions.length) {
                        for (final subscription in subscriptions) {
                          subscription.cancel();
                        }
                        completer.complete();
                      }
                    }
                  }

                  subscriptions = [
                    _personalDataBloc.reloading.listen(finish),
                    _dataUsageBloc.reloading.listen(finish),
                  ];

                  return completer.future;
                },
                child: ListView(
                  padding: const EdgeInsets.only(
                    top: 55,
                    left: 20,
                    right: 20,
                    bottom: 20,
                  ),
                  children: [
                    RxResultBuilder<PersonalDataBloc, PersonalData>(
                      bloc: _personalDataBloc,
                      state: (final bloc) => bloc.data as BehaviorSubject<Result<PersonalData>>,
                      buildSuccess: (final context, final personalData, final bloc) => _buildCard(
                        context: context,
                        title: AppLocalizations.of(context).myPersonalData,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              personalData.fullName,
                              style: Theme.of(context).textTheme.subtitle2!.copyWith(
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                            if (personalData.birthDate != null) ...[
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                '${AppLocalizations.of(context).born} ${dateFormat(personalData.birthDate!.value)}',
                                style: const TextStyle(
                                  color: valueLabelColor,
                                ),
                              ),
                            ],
                          ],
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (final _) => const PersonalDataPage()));
                        },
                      ),
                      buildError: (final context, final error, final bloc) => _buildCard(
                        context: context,
                        title: AppLocalizations.of(context).myPersonalData,
                        child: const Icon(Icons.error),
                      ),
                      buildLoading: (final context, final bloc) => _buildCard(
                        context: context,
                        title: AppLocalizations.of(context).myPersonalData,
                        child: const LoadingOrLinkIndicator(),
                      ),
                    ),
                    RxBlocBuilder<SessionBloc, bool>(
                      state: (final bloc) => bloc.isLoggedIn,
                      builder: (final context, final snapshot, final bloc) => snapshot.hasData && snapshot.data!
                          ? RxResultBuilder<DataUsageBloc, DataUsage>(
                              bloc: _dataUsageBloc,
                              state: (final bloc) => bloc.data as BehaviorSubject<Result<DataUsage>>,
                              buildSuccess: (final context, final dataUsage, final bloc) => _buildCard(
                                context: context,
                                title: AppLocalizations.of(context).whoUsesMyData,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      dataUsage.requests[0].organization,
                                      style: Theme.of(context).textTheme.subtitle2!.copyWith(
                                            fontWeight: FontWeight.bold,
                                          ),
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      dateFormat(dataUsage.requests[0].date),
                                      style: const TextStyle(
                                        color: valueLabelColor,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      dataUsage.requests[0].reasonOfUse,
                                      style: const TextStyle(
                                        color: valueLabelColor,
                                      ),
                                    ),
                                  ],
                                ),
                                onTap: () {
                                  Navigator.of(context)
                                      .push(MaterialPageRoute(builder: (final _) => const DataUsagePage()));
                                },
                              ),
                              buildError: (final context, final error, final bloc) => _buildCard(
                                context: context,
                                title: AppLocalizations.of(context).whoUsesMyData,
                                child: const Icon(Icons.error),
                              ),
                              buildLoading: (final context, final bloc) => _buildCard(
                                context: context,
                                title: AppLocalizations.of(context).whoUsesMyData,
                                child: const LoadingOrLinkIndicator(),
                              ),
                            )
                          : Container(),
                    ),
                    _buildCard(
                      context: context,
                      title: AppLocalizations.of(context).selfService,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () {},
                            child: Text(
                              AppLocalizations.of(context).passportOrIDCard,
                              style: const TextStyle(
                                color: primaryButtonColor,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Text(
                              AppLocalizations.of(context).move,
                              style: const TextStyle(
                                color: primaryButtonColor,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                        ],
                      ),
                      onTap: () {},
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  Widget _buildCard({
    required final BuildContext context,
    required final String title,
    required final Widget child,
    final Function()? onTap,
  }) =>
      GestureDetector(
        onTap: onTap,
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      title,
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(color: iconColor),
                    ),
                    if (onTap != null) ...[
                      const Icon(
                        Icons.arrow_right_sharp,
                        size: 28,
                      ),
                    ] else ...[
                      const SizedBox(
                        height: 28,
                      ),
                    ],
                  ],
                ),
              ),
              Container(
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      color: Color.fromARGB(50, 0, 0, 0),
                    ),
                  ),
                ),
                padding: const EdgeInsets.all(20),
                child: child,
              ),
            ],
          ),
        ),
      );
}
