import 'package:flutter/material.dart';
import 'package:intersperse/intersperse.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/model/property.dart';
import 'package:mijngegevensapp/utils/formats.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/page_wrapper.dart';
import 'package:mijngegevensapp/widgets/secondary_button.dart';
import 'package:mijngegevensapp/widgets/value_field.dart';

class PropertiesDetailsPage extends StatelessWidget {
  const PropertiesDetailsPage({
    required final this.title,
    required final this.properties,
    required final this.onErrorPage,
    final this.icon,
    final this.effectiveDate,
    final this.registeredBy,
    final this.inclusionDate,
    final this.eventID,
    final Key? key,
  }) : super(key: key);

  final String title;
  final List<Property> properties;
  final Function() onErrorPage;
  final IconData? icon;
  final DateTime? effectiveDate;
  final String? registeredBy;
  final DateTime? inclusionDate;
  final String? eventID;

  @override
  Widget build(final BuildContext context) => PageWrapper(
        sheetPage: true,
        title: title,
        icon: icon,
        body: Expanded(
          child: ListView(
            padding: const EdgeInsets.all(20),
            children: [
              for (final property in properties) ...[
                ValueField(
                  label: property.getLabel(context),
                  value: property.getDisplayValue(context),
                ),
              ],
              if (effectiveDate != null || registeredBy != null || inclusionDate != null) ...[
                Column(
                  children: [
                    const Divider(
                      color: categoryColor,
                      thickness: 2,
                    ),
                    if (effectiveDate != null) ...[
                      ValueField(
                        label: AppLocalizations.of(context).effectiveDate(title.uncapitalized),
                        value: dateFormat(effectiveDate!),
                      ),
                    ],
                    if (registeredBy != null) ...[
                      ValueField(
                        label: AppLocalizations.of(context).registeredBy,
                        value: AppLocalizations.of(context).municipality(' ${registeredBy!}'),
                      ),
                    ],
                    if (inclusionDate != null) ...[
                      if (DateTime(inclusionDate!.year, inclusionDate!.month, inclusionDate!.day)
                          .isAtSameMomentAs(inclusionDate!)) ...[
                        ValueField(
                          label: AppLocalizations.of(context).inclusionDate,
                          value: dateFormat(inclusionDate!),
                        ),
                      ] else ...[
                        ValueField(
                          label: AppLocalizations.of(context).inclusionDateAndTime,
                          value: dateTimeFormat(inclusionDate!),
                        ),
                      ],
                    ],
                    if (eventID != null) ...[
                      ValueField(
                        label: AppLocalizations.of(context).eventID,
                        value: eventID,
                      ),
                    ],
                    const Divider(
                      color: categoryColor,
                      thickness: 2,
                    ),
                  ]
                      .intersperse(
                        Container(
                          height: 20,
                        ),
                      )
                      .toList(),
                ),
              ],
              Text(
                AppLocalizations.of(context).reportErrorNote,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: valueLabelColor,
                  fontSize: 14,
                ),
              ),
              SecondaryButton(
                onPressed: onErrorPage,
                child: Text(AppLocalizations.of(context).reportError),
              ),
            ]
                .intersperse(
                  Container(
                    height: 30,
                  ),
                )
                .toList(),
          ),
        ),
      );
}
