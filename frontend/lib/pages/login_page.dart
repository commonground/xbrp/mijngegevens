import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:intersperse/intersperse.dart';
import 'package:mijngegevensapp/bloc/session_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/page_wrapper.dart';
import 'package:mijngegevensapp/widgets/primary_button.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({final Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String? _selectedMunicipality;

  @override
  Widget build(final BuildContext context) => PageWrapper(
        title: AppLocalizations.of(context).linkWithMunicipality,
        sheetPage: true,
        body: Expanded(
          child: ListView(
            padding: const EdgeInsets.all(20),
            children: [
              Text(
                AppLocalizations.of(context).linkInstruction,
                style: const TextStyle(
                  fontSize: 16,
                  height: 1.5,
                ),
              ),
              Container(
                color: backgroundColor,
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: DropdownButton<String>(
                  hint: Text(AppLocalizations.of(context).residentialMunicipality),
                  underline: Container(
                    color: textAndValueColor,
                    height: 1,
                  ),
                  isExpanded: true,
                  value: _selectedMunicipality,
                  onChanged: (final m) {
                    setState(() {
                      _selectedMunicipality = m;
                    });
                  },
                  items: [
                    // TODO: Load these from the client?
                    'Bovenhove',
                    'Groenestede',
                    'Heidedal',
                    'Oudescha',
                  ]
                      .map<DropdownMenuItem<String>>(
                        (final m) => DropdownMenuItem(
                          value: m,
                          child: Text(m),
                        ),
                      )
                      .toList(),
                ),
              ),
              PrimaryButton(
                onPressed: _selectedMunicipality == null
                    ? null
                    : () {
                        RxBlocProvider.of<SessionBloc>(context).setIsLoggedIn();
                        Navigator.pop(context);
                      },
                child: Text(
                  AppLocalizations.of(context).linkWithMunicipality,
                ),
              ),
            ]
                .intersperse(
                  Container(
                    height: 25,
                  ),
                )
                .toList(),
          ),
        ),
      );
}
