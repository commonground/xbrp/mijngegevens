import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:intersperse/intersperse.dart';
import 'package:mijngegevensapp/bloc/personal_data_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/model/data_request.dart';
import 'package:mijngegevensapp/model/personal_data.dart';
import 'package:mijngegevensapp/utils/formats.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/information_type_box.dart';
import 'package:mijngegevensapp/widgets/loading_or_link_indicator.dart';
import 'package:mijngegevensapp/widgets/page_wrapper.dart';
import 'package:mijngegevensapp/widgets/value_field.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

class DataUsageDetailsPage extends StatelessWidget {
  const DataUsageDetailsPage({
    required final this.request,
    final Key? key,
  }) : super(key: key);

  final DataRequest request;

  @override
  Widget build(final BuildContext context) => PageWrapper(
        sheetPage: true,
        title: AppLocalizations.of(context).dataUsage,
        body: RxResultBuilder<PersonalDataBloc, PersonalData>(
          state: (final bloc) => bloc.data as BehaviorSubject<Result<PersonalData>>,
          buildSuccess: (final context, final personalData, final bloc) => Expanded(
            child: ListView(
              padding: const EdgeInsets.all(20),
              children: <Widget>[
                ValueField(
                  label: AppLocalizations.of(context).date,
                  value: dateFormat(request.date),
                ),
                ValueField(
                  label: AppLocalizations.of(context).organization,
                  value: request.organization,
                ),
                ValueField(
                  label: AppLocalizations.of(context).reasonOfUse,
                  value: request.reasonOfUse,
                ),
                ValueField(
                  label: AppLocalizations.of(context).reason,
                  value: request.reason,
                ),
                const Divider(
                  color: categoryColor,
                  thickness: 2,
                ),
                Text(
                  AppLocalizations.of(context).dataUsed,
                  style: Theme.of(context).textTheme.headline6,
                ),
                for (final info in request.accessedInformation) ...[
                  InformationTypeBox(
                    info: info,
                  ),
                  if (info == InformationType.basicInformation) ...[
                    for (final property in personalData.basicDataProperties) ...[
                      ValueField(
                        label: property.getLabel(context),
                        value: property.getDisplayValue(context),
                      ),
                    ],
                  ],
                  if (info == InformationType.residentialAddress) ...[
                    for (final property in personalData.residentialAddressProperties) ...[
                      ValueField(
                        label: property.getLabel(context),
                        value: property.getDisplayValue(context),
                      ),
                    ],
                  ],
                ],
                const Divider(
                  color: categoryColor,
                  thickness: 2,
                ),
                Text(
                  AppLocalizations.of(context).wantToKnowMore,
                  style: Theme.of(context).textTheme.headline6,
                ),
                Text(
                  AppLocalizations.of(context).organizationWillInformYou(request.organization),
                ),
                const Divider(
                  color: categoryColor,
                  thickness: 2,
                ),
              ]
                  .intersperse(
                    Container(
                      height: 20,
                    ),
                  )
                  .toList(),
            ),
          ),
          buildError: (final context, final error, final bloc) => const Icon(Icons.error),
          buildLoading: (final context, final bloc) => Container(
            padding: const EdgeInsets.all(10),
            child: const LoadingOrLinkIndicator(),
          ),
        ),
      );
}
