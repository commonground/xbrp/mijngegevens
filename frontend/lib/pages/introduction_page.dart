import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:intersperse/intersperse.dart';
import 'package:mijngegevensapp/bloc/introduction_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/primary_button.dart';

class IntroductionPage extends StatelessWidget {
  IntroductionPage({
    final Key? key,
  }) : super(key: key);

  final _scrollController = ScrollController();

  @override
  Widget build(final BuildContext context) => Scaffold(
        body: Column(
          children: [
            ClipRect(
              child: Align(
                alignment: Alignment.topCenter,
                heightFactor: 0.925,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  heightFactor: 0.925,
                  child: Image.asset('assets/introduction_background.jpg'),
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height - 377,
              child: Stack(
                children: [
                  Scrollbar(
                    controller: _scrollController,
                    isAlwaysShown: true,
                    child: ListView(
                      controller: _scrollController,
                      padding: EdgeInsets.zero,
                      children: [
                        Container(
                          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 25),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(bottom: 25),
                                child: Text(
                                  AppLocalizations.of(context).introductionTitle,
                                  style: Theme.of(context).textTheme.subtitle1!.copyWith(
                                        fontSize: 24,
                                        color: textAndValueColor,
                                      ),
                                ),
                              ),
                              Column(
                                children: <Widget>[
                                  for (final line in AppLocalizations.of(context).introductionText.split('\n')) ...[
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        if (AppLocalizations.of(context).introductionText.split('\n').indexOf(line) !=
                                            0) ...[
                                          Container(
                                            margin: const EdgeInsets.all(10),
                                            child: ClipOval(
                                              child: Container(
                                                height: 6,
                                                width: 6,
                                                color: const Color(0xFF4F4F4F),
                                              ),
                                            ),
                                          ),
                                        ],
                                        Flexible(
                                          child: Text(
                                            line,
                                            style: Theme.of(context).textTheme.subtitle1!.copyWith(
                                                  fontSize: 16,
                                                  color: const Color(0xFF4F4F4F),
                                                  height: 1.5,
                                                  letterSpacing: 0.75,
                                                ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ]
                                    .intersperse(
                                      Container(
                                        height: 10,
                                      ),
                                    )
                                    .toList(),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: RotatedBox(
                        quarterTurns: 1,
                        child: IconButton(
                          onPressed: () {
                            _scrollController.animateTo(
                              _scrollController.position.maxScrollExtent,
                              duration: const Duration(milliseconds: 200),
                              curve: Curves.easeInOut,
                            );
                          },
                          icon: const Icon(
                            Icons.arrow_right_sharp,
                            color: iconColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 25,
                vertical: 10,
              ),
              child: PrimaryButton(
                onPressed: () {
                  RxBlocProvider.of<IntroductionBloc>(context).setHasSeen();
                  Navigator.of(context).pop();
                },
                child: Text(AppLocalizations.of(context).introductionButton),
              ),
            ),
            Container(
              color: backgroundColor,
              padding: const EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 25,
              ),
              child: Text(
                AppLocalizations.of(context).introductionNote,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 14,
                  color: Color(0xFF4F4F4F),
                  height: 1.5,
                  letterSpacing: 0.75,
                ),
              ),
            ),
          ],
        ),
      );
}
