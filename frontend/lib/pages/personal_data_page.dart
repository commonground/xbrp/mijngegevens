import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:intersperse/intersperse.dart';
import 'package:mijngegevensapp/bloc/personal_data_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/model/personal_data.dart';
import 'package:mijngegevensapp/model/property.dart';
import 'package:mijngegevensapp/pages/property_details_page.dart';
import 'package:mijngegevensapp/pages/report_error_page.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/loading_or_link_indicator.dart';
import 'package:mijngegevensapp/widgets/page_wrapper.dart';
import 'package:mijngegevensapp/widgets/secondary_button.dart';
import 'package:mijngegevensapp/widgets/value_field.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

class PersonalDataPage extends StatelessWidget {
  const PersonalDataPage({
    final Key? key,
  }) : super(key: key);

  @override
  Widget build(final BuildContext context) => PageWrapper(
        title: AppLocalizations.of(context).myPersonalData,
        body: Container(
          color: backgroundColor,
          child: ListView(
            padding: const EdgeInsets.all(20),
            children: [
              RxResultBuilder<PersonalDataBloc, PersonalData>(
                state: (final bloc) => bloc.data as BehaviorSubject<Result<PersonalData>>,
                buildSuccess: (final context, final personalData, final bloc) => Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _buildSection(
                      context: context,
                      title: personalData.fullName,
                      children: [
                        _buildCard(
                          context: context,
                          title: AppLocalizations.of(context).basicData,
                          children: _buildPropertyList(
                            context: context,
                            properties: personalData.basicDataProperties,
                            onTap: (final property) => _openPropertyDetailsPage(
                              context,
                              property,
                              personalData.municipality.value,
                            ),
                          )
                              .intersperse(
                                Container(
                                  height: 15,
                                ),
                              )
                              .toList(),
                        ),
                        if (personalData.residentialAddress != null) ...[
                          _buildCard(
                            context: context,
                            title: AppLocalizations.of(context).residentialAddress,
                            children: [
                              ..._buildPropertyList(
                                context: context,
                                properties: personalData.residentialAddressProperties,
                                onTap: (final property) => _openPropertyDetailsPage(
                                  context,
                                  property,
                                  personalData.municipality.value,
                                ),
                              ),
                              Column(
                                children: [
                                  SecondaryButton(
                                    child: Text(AppLocalizations.of(context).notifyOfRelocation),
                                    onPressed: () {},
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  SecondaryButton(
                                    child: Text(AppLocalizations.of(context).viewPreviousAddresses),
                                    onPressed: () {},
                                  ),
                                ],
                              ),
                            ]
                                .intersperse(
                                  Container(
                                    height: 15,
                                  ),
                                )
                                .toList(),
                          ),
                        ],
                      ],
                    ),
                    if (personalData.parents.isNotEmpty) ...[
                      _buildSection(
                        context: context,
                        title: AppLocalizations.of(context).parents,
                        children: [
                          for (final parent in personalData.parents) ...[
                            _buildCard(
                              context: context,
                              title: parent.fullName,
                              children: _buildPropertyList(
                                context: context,
                                properties: parent.parentProperties,
                              )
                                  .intersperse(
                                    Container(
                                      height: 15,
                                    ),
                                  )
                                  .toList(),
                            ),
                          ],
                        ],
                      ),
                    ],
                    if (personalData.relationship != null) ...[
                      _buildSection(
                        context: context,
                        title: AppLocalizations.of(context).relationship,
                        children: [
                          _buildCard(
                            context: context,
                            title: personalData.relationship!.fullName,
                            children: _buildPropertyList(
                              context: context,
                              properties: personalData.relationship!.relationshipProperties,
                            )
                                .intersperse(
                                  Container(
                                    height: 15,
                                  ),
                                )
                                .toList(),
                          ),
                        ],
                      ),
                    ],
                    if (personalData.children.isNotEmpty) ...[
                      _buildSection(
                        context: context,
                        title: AppLocalizations.of(context).children,
                        children: [
                          for (final child in personalData.children) ...[
                            _buildCard(
                              context: context,
                              title: child.fullName,
                              children: _buildPropertyList(
                                context: context,
                                properties: child.childProperties,
                              )
                                  .intersperse(
                                    Container(
                                      height: 15,
                                    ),
                                  )
                                  .toList(),
                            ),
                          ],
                        ],
                      ),
                    ],
                  ]
                      .intersperse(
                        Container(
                          height: 10,
                        ),
                      )
                      .toList(),
                ),
                buildError: (final context, final error, final bloc) => const Icon(Icons.error),
                buildLoading: (final context, final bloc) => Container(
                  padding: const EdgeInsets.all(10),
                  child: const LoadingOrLinkIndicator(),
                ),
              ),
            ],
          ),
        ),
      );

  Future<DateTime?> _openPropertyDetailsPage(
    final BuildContext context,
    final Property property,
    final String municipality,
  ) =>
      Navigator.of(context).push<DateTime>(
        MaterialPageRoute(
          builder: (final context) => PropertiesDetailsPage(
            properties: [property],
            title: property.getLabel(context),
            onErrorPage: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (final _) => ReportErrorPage(
                    title: property.getLabel(context),
                    properties: [property],
                    municipality: municipality,
                  ),
                ),
              );
            },
            effectiveDate: property.effectiveDate,
            registeredBy: property.registeredBy,
            inclusionDate: property.inclusionDate,
          ),
        ),
      );

  List<Widget> _buildPropertyList({
    required final BuildContext context,
    required final List<Property?> properties,
    final Future<DateTime?> Function(Property property)? onTap,
  }) =>
      properties
          .where((final property) => property != null)
          .map<Widget>(
            (final property) => ValueField(
              label: property!.getLabel(context),
              value: property.getDisplayValue(context),
              onTap: onTap != null ? () => onTap(property) : null,
            ),
          )
          .toList();

  Widget _buildSection({
    required final BuildContext context,
    required final String title,
    required final List<Widget> children,
  }) =>
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.headline5!.copyWith(
                  color: Colors.black,
                ),
          ),
          ...children,
        ],
      );

  Widget _buildCard({
    required final BuildContext context,
    required final String title,
    required final List<Widget> children,
  }) =>
      Card(
        child: ExpansionTile(
          title: Text(
            title,
            style: Theme.of(context).textTheme.headline6,
          ),
          collapsedTextColor: textAndValueColor,
          collapsedIconColor: textAndValueColor,
          iconColor: textAndValueColor,
          textColor: textAndValueColor,
          expandedAlignment: Alignment.topLeft,
          childrenPadding: const EdgeInsets.only(
            left: 16,
            right: 16,
            bottom: 16,
          ),
          children: children,
        ),
      );
}
