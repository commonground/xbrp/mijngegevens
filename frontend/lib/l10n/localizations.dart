import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'localizations_nl.dart';

/// Callers can lookup localized strings with an instance of AppLocalizations returned
/// by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// localizationDelegates list, and the locales they support in the app's
/// supportedLocales list. For example:
///
/// ```
/// import 'l10n/localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations)!;
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[Locale('nl')];

  /// No description provided for @title.
  ///
  /// In nl, this message translates to:
  /// **'Mijngegevens app'**
  String get title;

  /// No description provided for @myPersonalData.
  ///
  /// In nl, this message translates to:
  /// **'Mijn persoonsgegevens'**
  String get myPersonalData;

  /// No description provided for @whoUsesMyData.
  ///
  /// In nl, this message translates to:
  /// **'Wie gebruiken mijn gegevens?'**
  String get whoUsesMyData;

  /// No description provided for @selfService.
  ///
  /// In nl, this message translates to:
  /// **'Zelf regelen'**
  String get selfService;

  /// No description provided for @born.
  ///
  /// In nl, this message translates to:
  /// **'geboren'**
  String get born;

  /// No description provided for @passportOrIDCard.
  ///
  /// In nl, this message translates to:
  /// **'Paspoort of identiteitskaart'**
  String get passportOrIDCard;

  /// No description provided for @move.
  ///
  /// In nl, this message translates to:
  /// **'Verhuizing'**
  String get move;

  /// No description provided for @history.
  ///
  /// In nl, this message translates to:
  /// **'Historie'**
  String get history;

  /// No description provided for @pendingCases.
  ///
  /// In nl, this message translates to:
  /// **'Lopende zaken'**
  String get pendingCases;

  /// No description provided for @settings.
  ///
  /// In nl, this message translates to:
  /// **'Instellingen'**
  String get settings;

  /// No description provided for @basicData.
  ///
  /// In nl, this message translates to:
  /// **'Basisgegevens'**
  String get basicData;

  /// No description provided for @residentialAddress.
  ///
  /// In nl, this message translates to:
  /// **'Woonadres'**
  String get residentialAddress;

  /// No description provided for @parents.
  ///
  /// In nl, this message translates to:
  /// **'Ouders'**
  String get parents;

  /// No description provided for @relationship.
  ///
  /// In nl, this message translates to:
  /// **'Relatie'**
  String get relationship;

  /// No description provided for @children.
  ///
  /// In nl, this message translates to:
  /// **'Kinderen'**
  String get children;

  /// No description provided for @firstName.
  ///
  /// In nl, this message translates to:
  /// **'Voornamen'**
  String get firstName;

  /// No description provided for @lastName.
  ///
  /// In nl, this message translates to:
  /// **'Geschlachtsnaam'**
  String get lastName;

  /// No description provided for @usedName.
  ///
  /// In nl, this message translates to:
  /// **'Naamgebruik'**
  String get usedName;

  /// No description provided for @bsn.
  ///
  /// In nl, this message translates to:
  /// **'Burgerservicenummer'**
  String get bsn;

  /// No description provided for @birthDate.
  ///
  /// In nl, this message translates to:
  /// **'Geboortedatum'**
  String get birthDate;

  /// No description provided for @birthPlace.
  ///
  /// In nl, this message translates to:
  /// **'Geboorteplaats'**
  String get birthPlace;

  /// No description provided for @birthCountry.
  ///
  /// In nl, this message translates to:
  /// **'Geboorteland'**
  String get birthCountry;

  /// No description provided for @dutchNationality.
  ///
  /// In nl, this message translates to:
  /// **'Nederlandse nationaliteit'**
  String get dutchNationality;

  /// No description provided for @yes.
  ///
  /// In nl, this message translates to:
  /// **'Ja'**
  String get yes;

  /// No description provided for @no.
  ///
  /// In nl, this message translates to:
  /// **'Geen'**
  String get no;

  /// No description provided for @ownName.
  ///
  /// In nl, this message translates to:
  /// **'Eigen naam'**
  String get ownName;

  /// No description provided for @municipality.
  ///
  /// In nl, this message translates to:
  /// **'Gemeente{municipality}'**
  String municipality(Object municipality);

  /// No description provided for @notifyOfRelocation.
  ///
  /// In nl, this message translates to:
  /// **'Verhuizing doorgeven'**
  String get notifyOfRelocation;

  /// No description provided for @viewPreviousAddresses.
  ///
  /// In nl, this message translates to:
  /// **'Eerdere adressen bekijken'**
  String get viewPreviousAddresses;

  /// No description provided for @reportError.
  ///
  /// In nl, this message translates to:
  /// **'Fout melden'**
  String get reportError;

  /// No description provided for @reportErrorNote.
  ///
  /// In nl, this message translates to:
  /// **'Als er een fout staat in uw gegevens, meld dit dan. De gemeente helpt de fout herstellen.'**
  String get reportErrorNote;

  /// No description provided for @effectiveDate.
  ///
  /// In nl, this message translates to:
  /// **'Ingangsdatum {label}'**
  String effectiveDate(Object label);

  /// No description provided for @inclusionDate.
  ///
  /// In nl, this message translates to:
  /// **'Datum van opneming'**
  String get inclusionDate;

  /// No description provided for @inclusionDateAndTime.
  ///
  /// In nl, this message translates to:
  /// **'Datum en tijd opneming'**
  String get inclusionDateAndTime;

  /// No description provided for @registeredBy.
  ///
  /// In nl, this message translates to:
  /// **'Geregistreerd door'**
  String get registeredBy;

  /// No description provided for @reportErrorInstructions.
  ///
  /// In nl, this message translates to:
  /// **'Geef aan wat volgens u fout geregistreerd staat, en wat de juiste informatie is. De gemeente {municipality} behandelt uw melding.'**
  String reportErrorInstructions(Object municipality);

  /// No description provided for @errorIn.
  ///
  /// In nl, this message translates to:
  /// **'Fout in ´{label}´'**
  String errorIn(Object label);

  /// No description provided for @yourMessage.
  ///
  /// In nl, this message translates to:
  /// **'Uw melding'**
  String get yourMessage;

  /// No description provided for @sendMessage.
  ///
  /// In nl, this message translates to:
  /// **'Melding verzenden'**
  String get sendMessage;

  /// No description provided for @errorReported.
  ///
  /// In nl, this message translates to:
  /// **'U heeft een fout gemeld op {date}'**
  String errorReported(Object date);

  /// No description provided for @errorWillBeProcessed.
  ///
  /// In nl, this message translates to:
  /// **'Uw foutmelding wordt in behandeling genomen door gemeente {municipality}.'**
  String errorWillBeProcessed(Object municipality);

  /// No description provided for @dataUsageInfo.
  ///
  /// In nl, this message translates to:
  /// **'U ziet hier welke overheidsorganisaties uw persoonsgegevens gebruiken en waarvoor.'**
  String get dataUsageInfo;

  /// No description provided for @dataUsed.
  ///
  /// In nl, this message translates to:
  /// **'Gebruikte gegevens'**
  String get dataUsed;

  /// No description provided for @dataUsage.
  ///
  /// In nl, this message translates to:
  /// **'Gegevensgebruik'**
  String get dataUsage;

  /// No description provided for @reasonOfUse.
  ///
  /// In nl, this message translates to:
  /// **'Reden gebruik'**
  String get reasonOfUse;

  /// No description provided for @reason.
  ///
  /// In nl, this message translates to:
  /// **'Aanleiding'**
  String get reason;

  /// No description provided for @date.
  ///
  /// In nl, this message translates to:
  /// **'Datum'**
  String get date;

  /// No description provided for @organization.
  ///
  /// In nl, this message translates to:
  /// **'Organisatie'**
  String get organization;

  /// No description provided for @wantToKnowMore.
  ///
  /// In nl, this message translates to:
  /// **'Meer weten?'**
  String get wantToKnowMore;

  /// No description provided for @organizationWillInformYou.
  ///
  /// In nl, this message translates to:
  /// **'{organization} informeert u over het besluit in dit proces.'**
  String organizationWillInformYou(Object organization);

  /// No description provided for @historyEntryDateMunicipality.
  ///
  /// In nl, this message translates to:
  /// **'Op {date} in {municipality}'**
  String historyEntryDateMunicipality(Object date, Object municipality);

  /// No description provided for @parent.
  ///
  /// In nl, this message translates to:
  /// **'Ouder'**
  String get parent;

  /// No description provided for @child.
  ///
  /// In nl, this message translates to:
  /// **'Kind'**
  String get child;

  /// No description provided for @effectiveDateParentChildRelationship.
  ///
  /// In nl, this message translates to:
  /// **'Ingangsdatum ouder-kind relatie'**
  String get effectiveDateParentChildRelationship;

  /// No description provided for @marriedTo.
  ///
  /// In nl, this message translates to:
  /// **'Gehuwd met'**
  String get marriedTo;

  /// No description provided for @marriageDate.
  ///
  /// In nl, this message translates to:
  /// **'Datum huwelijk'**
  String get marriageDate;

  /// No description provided for @marriagePlace.
  ///
  /// In nl, this message translates to:
  /// **'Plaats huwelijk'**
  String get marriagePlace;

  /// No description provided for @marriageCountry.
  ///
  /// In nl, this message translates to:
  /// **'Land huwelijk'**
  String get marriageCountry;

  /// No description provided for @partner1.
  ///
  /// In nl, this message translates to:
  /// **'Partner 1'**
  String get partner1;

  /// No description provided for @partner2.
  ///
  /// In nl, this message translates to:
  /// **'Partner 2'**
  String get partner2;

  /// No description provided for @weddingDate.
  ///
  /// In nl, this message translates to:
  /// **'Trouwdatum'**
  String get weddingDate;

  /// No description provided for @eventID.
  ///
  /// In nl, this message translates to:
  /// **'EventID'**
  String get eventID;

  /// No description provided for @introductionTitle.
  ///
  /// In nl, this message translates to:
  /// **'Welkom in uw persoonlijke Mijngegevens app'**
  String get introductionTitle;

  /// No description provided for @introductionText.
  ///
  /// In nl, this message translates to:
  /// **'Met deze app kunt u:\nZien welke persoonsgegevens de gemeente over u heeft geregistreerd.\nZien door wie die gegevens worden gebruikt.\nEventuele fouten in uw gegevens melden.\nWijzigingen in uw situatie doorgeven.\nAndere zaken met uw gemeente regelen.'**
  String get introductionText;

  /// No description provided for @introductionNote.
  ///
  /// In nl, this message translates to:
  /// **'Mijngegevens app is ontwikkeld door de Nederlandse gemeenten en de Rijksoverheid samen'**
  String get introductionNote;

  /// No description provided for @introductionButton.
  ///
  /// In nl, this message translates to:
  /// **'Doorgaan'**
  String get introductionButton;

  /// No description provided for @linkWithMunicipality.
  ///
  /// In nl, this message translates to:
  /// **'Koppel met gemeente'**
  String get linkWithMunicipality;

  /// No description provided for @linkExplanation.
  ///
  /// In nl, this message translates to:
  /// **'Koppel de app eenmalig met uw gemeente, zodat u hier altijd uw persoonsgegevens kunt inzien.'**
  String get linkExplanation;

  /// No description provided for @linkInstruction.
  ///
  /// In nl, this message translates to:
  /// **'Selecteer uw woongemeente en klik op ‘Koppelen’. U komt dan op de website van uw gemeente waar u kunt inloggen met DigiD.'**
  String get linkInstruction;

  /// No description provided for @residentialMunicipality.
  ///
  /// In nl, this message translates to:
  /// **'Woongemeente'**
  String get residentialMunicipality;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['nl'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {
  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'nl':
      return AppLocalizationsNl();
  }

  throw FlutterError('AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
      'an issue with the localizations generation tool. Please file an issue '
      'on GitHub with a reproducible sample app and the gen-l10n configuration '
      'that was used.');
}
