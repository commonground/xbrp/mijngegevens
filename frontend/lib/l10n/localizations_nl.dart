import 'localizations.dart';

/// The translations for Dutch Flemish (`nl`).
class AppLocalizationsNl extends AppLocalizations {
  AppLocalizationsNl([String locale = 'nl']) : super(locale);

  @override
  String get title => 'Mijngegevens app';

  @override
  String get myPersonalData => 'Mijn persoonsgegevens';

  @override
  String get whoUsesMyData => 'Wie gebruiken mijn gegevens?';

  @override
  String get selfService => 'Zelf regelen';

  @override
  String get born => 'geboren';

  @override
  String get passportOrIDCard => 'Paspoort of identiteitskaart';

  @override
  String get move => 'Verhuizing';

  @override
  String get history => 'Historie';

  @override
  String get pendingCases => 'Lopende zaken';

  @override
  String get settings => 'Instellingen';

  @override
  String get basicData => 'Basisgegevens';

  @override
  String get residentialAddress => 'Woonadres';

  @override
  String get parents => 'Ouders';

  @override
  String get relationship => 'Relatie';

  @override
  String get children => 'Kinderen';

  @override
  String get firstName => 'Voornamen';

  @override
  String get lastName => 'Geschlachtsnaam';

  @override
  String get usedName => 'Naamgebruik';

  @override
  String get bsn => 'Burgerservicenummer';

  @override
  String get birthDate => 'Geboortedatum';

  @override
  String get birthPlace => 'Geboorteplaats';

  @override
  String get birthCountry => 'Geboorteland';

  @override
  String get dutchNationality => 'Nederlandse nationaliteit';

  @override
  String get yes => 'Ja';

  @override
  String get no => 'Geen';

  @override
  String get ownName => 'Eigen naam';

  @override
  String municipality(Object municipality) {
    return 'Gemeente${municipality}';
  }

  @override
  String get notifyOfRelocation => 'Verhuizing doorgeven';

  @override
  String get viewPreviousAddresses => 'Eerdere adressen bekijken';

  @override
  String get reportError => 'Fout melden';

  @override
  String get reportErrorNote =>
      'Als er een fout staat in uw gegevens, meld dit dan. De gemeente helpt de fout herstellen.';

  @override
  String effectiveDate(Object label) {
    return 'Ingangsdatum $label';
  }

  @override
  String get inclusionDate => 'Datum van opneming';

  @override
  String get inclusionDateAndTime => 'Datum en tijd opneming';

  @override
  String get registeredBy => 'Geregistreerd door';

  @override
  String reportErrorInstructions(Object municipality) {
    return 'Geef aan wat volgens u fout geregistreerd staat, en wat de juiste informatie is. De gemeente $municipality behandelt uw melding.';
  }

  @override
  String errorIn(Object label) {
    return 'Fout in ´$label´';
  }

  @override
  String get yourMessage => 'Uw melding';

  @override
  String get sendMessage => 'Melding verzenden';

  @override
  String errorReported(Object date) {
    return 'U heeft een fout gemeld op $date';
  }

  @override
  String errorWillBeProcessed(Object municipality) {
    return 'Uw foutmelding wordt in behandeling genomen door gemeente $municipality.';
  }

  @override
  String get dataUsageInfo => 'U ziet hier welke overheidsorganisaties uw persoonsgegevens gebruiken en waarvoor.';

  @override
  String get dataUsed => 'Gebruikte gegevens';

  @override
  String get dataUsage => 'Gegevensgebruik';

  @override
  String get reasonOfUse => 'Reden gebruik';

  @override
  String get reason => 'Aanleiding';

  @override
  String get date => 'Datum';

  @override
  String get organization => 'Organisatie';

  @override
  String get wantToKnowMore => 'Meer weten?';

  @override
  String organizationWillInformYou(Object organization) {
    return '$organization informeert u over het besluit in dit proces.';
  }

  @override
  String historyEntryDateMunicipality(Object date, Object municipality) {
    return 'Op $date in $municipality';
  }

  @override
  String get parent => 'Ouder';

  @override
  String get child => 'Kind';

  @override
  String get effectiveDateParentChildRelationship => 'Ingangsdatum ouder-kind relatie';

  @override
  String get marriedTo => 'Gehuwd met';

  @override
  String get marriageDate => 'Datum huwelijk';

  @override
  String get marriagePlace => 'Plaats huwelijk';

  @override
  String get marriageCountry => 'Land huwelijk';

  @override
  String get partner1 => 'Partner 1';

  @override
  String get partner2 => 'Partner 2';

  @override
  String get weddingDate => 'Trouwdatum';

  @override
  String get eventID => 'EventID';

  @override
  String get introductionTitle => 'Welkom in uw persoonlijke Mijngegevens app';

  @override
  String get introductionText =>
      'Met deze app kunt u:\nZien welke persoonsgegevens de gemeente over u heeft geregistreerd.\nZien door wie die gegevens worden gebruikt.\nEventuele fouten in uw gegevens melden.\nWijzigingen in uw situatie doorgeven.\nAndere zaken met uw gemeente regelen.';

  @override
  String get introductionNote =>
      'Mijngegevens app is ontwikkeld door de Nederlandse gemeenten en de Rijksoverheid samen';

  @override
  String get introductionButton => 'Doorgaan';

  @override
  String get linkWithMunicipality => 'Koppel met gemeente';

  @override
  String get linkExplanation =>
      'Koppel de app eenmalig met uw gemeente, zodat u hier altijd uw persoonsgegevens kunt inzien.';

  @override
  String get linkInstruction =>
      'Selecteer uw woongemeente en klik op ‘Koppelen’. U komt dan op de website van uw gemeente waar u kunt inloggen met DigiD.';

  @override
  String get residentialMunicipality => 'Woongemeente';
}
