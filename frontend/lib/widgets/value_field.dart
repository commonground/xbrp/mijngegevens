import 'package:flutter/material.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/utils/formats.dart';

class ValueField extends StatefulWidget {
  const ValueField({
    required final this.label,
    required final this.value,
    final this.onTap,
    final Key? key,
  })  : assert(value is String || value is Widget, 'value needs to be either a String or a Widget'),
        super(key: key);

  final String label;
  final dynamic value;
  final Future<DateTime?> Function()? onTap;

  @override
  State<ValueField> createState() => _ValueFieldState();
}

class _ValueFieldState extends State<ValueField> {
  DateTime? _errorReportedDate;

  @override
  Widget build(final BuildContext context) => GestureDetector(
        onTap: () {
          if (widget.onTap != null) {
            widget.onTap!().then((final date) {
              if (mounted) {
                setState(() {
                  _errorReportedDate = date;
                });
              }
            });
          }
        },
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.label,
                        style: const TextStyle(
                          fontSize: 13,
                          color: Color(0xFF767676),
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      if (widget.value is String) ...[
                        Text(
                          widget.value as String,
                          style: const TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ] else ...[
                        widget.value as Widget,
                      ],
                    ],
                  ),
                ),
                if (widget.onTap != null) ...[
                  const Icon(Icons.zoom_in),
                ],
              ],
            ),
            if (_errorReportedDate != null) ...[
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  children: [
                    const Icon(
                      Icons.error,
                      color: Colors.orange,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Text(
                        AppLocalizations.of(context).errorReported(dateFormat(_errorReportedDate!)),
                        style: const TextStyle(
                          color: Colors.orange,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ],
        ),
      );
}
