import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:mijngegevensapp/bloc/session_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/pages/login_page.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/secondary_button.dart';

class LoadingOrLinkIndicator extends StatelessWidget {
  const LoadingOrLinkIndicator({final Key? key}) : super(key: key);

  @override
  Widget build(final BuildContext context) => RxBlocBuilder<SessionBloc, bool>(
        state: (final bloc) => bloc.isLoggedIn,
        builder: (final context, final snapshot, final bloc) => snapshot.hasData && snapshot.data!
            ? const LinearProgressIndicator()
            : Column(
                children: [
                  Text(
                    AppLocalizations.of(context).linkExplanation,
                    style: const TextStyle(
                      fontSize: 16,
                      color: textAndValueColor,
                      height: 1.5,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  SecondaryButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (final context) => const LoginPage(),
                        ),
                      );
                    },
                    child: Text(AppLocalizations.of(context).linkWithMunicipality),
                  ),
                ],
              ),
      );
}
