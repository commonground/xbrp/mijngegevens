import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:mijngegevensapp/bloc/session_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/pages/data_usage_page.dart';
import 'package:mijngegevensapp/pages/history_page.dart';
import 'package:mijngegevensapp/pages/personal_data_page.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:mijngegevensapp/widgets/profile_picture.dart';

class PageWrapper extends StatelessWidget {
  const PageWrapper({
    required final this.title,
    required final this.body,
    final this.icon,
    final this.sheetPage = false,
    final this.showDrawer = false,
    final Key? key,
  }) : super(key: key);

  final String title;
  final Widget body;
  final IconData? icon;
  final bool sheetPage;
  final bool showDrawer;

  @override
  Widget build(final BuildContext context) {
    if (sheetPage) {
      return Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: Container(
                  margin: const EdgeInsets.all(10),
                  child: GestureDetector(
                    child: const Icon(
                      Icons.close,
                      color: textAndValueColor,
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (icon != null) ...[
                    Icon(icon),
                    const SizedBox(
                      width: 5,
                    ),
                  ],
                  Text(
                    title,
                    style: Theme.of(context).textTheme.headline5,
                  ),
                ],
              ),
              const Divider(
                thickness: 1,
                color: Colors.black,
              ),
              const SizedBox(
                height: 10,
              ),
              body,
            ],
          ),
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (icon != null) ...[
              Icon(icon),
              const SizedBox(
                width: 5,
              ),
            ],
            Text(
              title,
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    color: whiteTextColor,
                    fontSize: 16,
                  ),
            ),
          ],
        ),
        actions: [
          RxBlocBuilder<SessionBloc, bool>(
            state: (final bloc) => bloc.isLoggedIn,
            builder: (final context, final snapshot, final bloc) => snapshot.hasData && snapshot.data!
                ? const ProfilePictureWidget()
                : const SizedBox(
                    width: 56,
                  ),
          ),
        ],
      ),
      body: SafeArea(
        child: body,
      ),
      drawer: showDrawer
          ? SafeArea(
              child: Drawer(
                child: Container(
                  color: primaryButtonColor,
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerRight,
                        child: IconButton(
                          icon: const Icon(
                            Icons.close,
                            color: whiteTextColor,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      _builDrawerItem(
                        context: context,
                        text: AppLocalizations.of(context).myPersonalData,
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (final context) => const PersonalDataPage(),
                            ),
                          );
                        },
                      ),
                      _builDrawerItem(
                        context: context,
                        text: AppLocalizations.of(context).whoUsesMyData,
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (final context) => const DataUsagePage(),
                            ),
                          );
                        },
                      ),
                      _builDrawerItem(
                        context: context,
                        text: AppLocalizations.of(context).history,
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (final context) => const HistoryPage(),
                            ),
                          );
                        },
                      ),
                      _builDrawerItem(
                        context: context,
                        text: AppLocalizations.of(context).selfService,
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      _builDrawerItem(
                        context: context,
                        text: AppLocalizations.of(context).pendingCases,
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      _builDrawerItem(
                        context: context,
                        text: AppLocalizations.of(context).settings,
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                ),
              ),
            )
          : null,
    );
  }

  Widget _builDrawerItem({
    required final BuildContext context,
    required final String text,
    required final Function() onTap,
  }) =>
      ListTile(
        title: Text(
          text,
          style: const TextStyle(
            color: whiteTextColor,
          ),
        ),
        onTap: onTap,
      );
}
