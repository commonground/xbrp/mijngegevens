import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:mijngegevensapp/bloc/profile_picture_bloc.dart';
import 'package:mijngegevensapp/model/profile_picture.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/subjects.dart';

class ProfilePictureWidget extends StatelessWidget {
  const ProfilePictureWidget({
    final Key? key,
  }) : super(key: key);

  @override
  Widget build(final BuildContext context) => SizedBox(
        width: 56,
        child: RxResultBuilder<ProfilePictureBloc, ProfilePicture>(
          state: (final bloc) => bloc.data as BehaviorSubject<Result<ProfilePicture>>,
          buildSuccess: (final context, final profilePicture, final bloc) => Container(
            padding: const EdgeInsets.all(10),
            child: ClipOval(
              child: Container(
                color: whiteTextColor,
                child: Image.memory(profilePicture.data),
              ),
            ),
          ),
          buildError: (final context, final error, final bloc) => const Icon(Icons.error),
          buildLoading: (final context, final bloc) => Container(
            padding: const EdgeInsets.all(18),
            child: const CircularProgressIndicator(
              strokeWidth: 2,
            ),
          ),
        ),
      );
}
