import 'package:flutter/material.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/model/data_request.dart';
import 'package:mijngegevensapp/utils/theme.dart';

class InformationTypeBox extends StatelessWidget {
  const InformationTypeBox({
    required final this.info,
    final Key? key,
  }) : super(key: key);

  final InformationType info;

  @override
  Widget build(final BuildContext context) => Container(
        decoration: const BoxDecoration(
          color: categoryColor,
          borderRadius: BorderRadius.all(Radius.circular(2)),
        ),
        padding: const EdgeInsets.all(7.5),
        margin: const EdgeInsets.symmetric(vertical: 5),
        child: Builder(
          builder: (final context) {
            late String text;
            switch (info) {
              case InformationType.basicInformation:
                text = AppLocalizations.of(context).basicData;
                break;
              case InformationType.residentialAddress:
                text = AppLocalizations.of(context).residentialAddress;
                break;
              default:
                throw Exception('Unknown type: $info');
            }
            return Text(
              text,
              style: const TextStyle(
                fontSize: 16,
              ),
            );
          },
        ),
      );
}
