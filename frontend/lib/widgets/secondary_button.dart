import 'package:flutter/material.dart';
import 'package:mijngegevensapp/utils/theme.dart';

class SecondaryButton extends StatelessWidget {
  const SecondaryButton({
    required final this.onPressed,
    required final this.child,
    final Key? key,
  }) : super(key: key);

  final Function() onPressed;
  final Widget child;

  @override
  Widget build(final BuildContext context) => ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: secondaryButtonColor,
          onPrimary: textAndValueColor,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          elevation: 4,
          minimumSize: const Size(double.infinity, 40),
        ),
        onPressed: onPressed,
        child: child,
      );
}
