import 'package:flutter/material.dart';
import 'package:mijngegevensapp/utils/theme.dart';

class PrimaryButton extends StatelessWidget {
  const PrimaryButton({
    required final this.child,
    final this.onPressed,
    final Key? key,
  }) : super(key: key);

  final Function()? onPressed;
  final Widget child;

  @override
  Widget build(final BuildContext context) => ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: primaryButtonColor,
          onPrimary: whiteTextColor,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          elevation: 4,
          minimumSize: const Size(double.infinity, 40),
        ),
        onPressed: onPressed,
        child: child,
      );
}
