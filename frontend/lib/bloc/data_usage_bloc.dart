import 'package:mijngegevensapp/api/client.dart';
import 'package:mijngegevensapp/bloc/generic_bloc.dart';
import 'package:mijngegevensapp/model/data_usage.dart';

class DataUsageBloc extends GenericBloc<DataUsage> {
  DataUsageBloc(final Client client) : super(client);
}
