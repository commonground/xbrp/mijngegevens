import 'package:mijngegevensapp/api/client.dart';
import 'package:mijngegevensapp/bloc/generic_bloc.dart';
import 'package:mijngegevensapp/model/personal_data.dart';

class PersonalDataBloc extends GenericBloc<PersonalData> {
  PersonalDataBloc(final Client client) : super(client);
}
