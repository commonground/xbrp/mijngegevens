import 'package:mijngegevensapp/api/client.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

part 'generic_bloc.rxb.g.dart';

abstract class GenericBlocEvents {
  void load();
}

abstract class GenericBlocStates<T> {
  BehaviorSubject<bool> get reloading;
  BehaviorSubject<Result<T>> get data;
}

@RxBloc()
class GenericBloc<T> extends $GenericBloc {
  GenericBloc(this.client);

  final Client client;

  T? _stored;
  final _subject = BehaviorSubject<Result<T>>();
  final _reloadingSubject = BehaviorSubject<bool>();

  @override
  void dispose() {
    super.dispose();
    _subject.close();
    _reloadingSubject.close();
  }

  @override
  BehaviorSubject<Result<T>> _mapToDataState() {
    _$loadEvent.flatMap<Result<T>>((final _) async* {
      if (_stored == null) {
        yield Result.loading();
      } else {
        _reloadingSubject.add(true);
      }
      try {
        final response = await client.get<T>();
        yield Result.success(response);
        if (_stored != null) {
          _reloadingSubject.add(false);
        }
        _stored = response;
      } catch (e, s) {
        print(e);
        print(s);
        yield Result.error(Exception());
      }
    }).listen(_subject.add);
    return _subject;
  }

  @override
  BehaviorSubject<bool> _mapToReloadingState() => _reloadingSubject;
}
