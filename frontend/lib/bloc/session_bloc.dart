import 'package:mijngegevensapp/api/preferences.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

part 'session_bloc.rxb.g.dart';

abstract class SessionBlocEvents {
  void checkIsLoggedIn();
  void setIsLoggedIn();
}

abstract class SessionBlocStates {
  BehaviorSubject<bool> get isLoggedIn;
}

@RxBloc()
class SessionBloc extends $SessionBloc {
  SessionBloc(this._preferences);

  final Preferences _preferences;

  final _subject = BehaviorSubject<bool>();

  @override
  void dispose() {
    _subject.close();
    super.dispose();
  }

  @override
  BehaviorSubject<bool> _mapToIsLoggedInState() {
    Rx.merge<bool>([
      _$checkIsLoggedInEvent.flatMap((final _) async* {
        yield _preferences.getIsLoggedIn();
      }),
      _$setIsLoggedInEvent.flatMap((final _) async* {
        _preferences.setIsLoggedIn();
        yield true;
      }),
    ]).listen(_subject.add);
    return _subject;
  }
}
