// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'report_error_bloc.dart';

/// Used as a contractor for the bloc, events and states classes
/// {@nodoc}
abstract class ReportErrorBlocType extends RxBlocTypeBase {
  ReportErrorBlocEvents get events;
  ReportErrorBlocStates get states;
}

/// [$ReportErrorBloc] extended by the [ReportErrorBloc]
/// {@nodoc}
abstract class $ReportErrorBloc extends RxBlocBase
    implements ReportErrorBlocEvents, ReportErrorBlocStates, ReportErrorBlocType {
  final _compositeSubscription = CompositeSubscription();

  /// Тhe [Subject] where events sink to by calling [send]
  final _$sendEvent = PublishSubject<_SendEventArgs>();

  /// The state of [sent] implemented in [_mapToSentState]
  late final BehaviorSubject<Result<bool>> _sentState = _mapToSentState();

  @override
  void send(List<Property<dynamic>> properties, String message) => _$sendEvent.add(_SendEventArgs(properties, message));

  @override
  BehaviorSubject<Result<bool>> get sent => _sentState;

  BehaviorSubject<Result<bool>> _mapToSentState();

  @override
  ReportErrorBlocEvents get events => this;

  @override
  ReportErrorBlocStates get states => this;

  @override
  void dispose() {
    _$sendEvent.close();
    _compositeSubscription.dispose();
    super.dispose();
  }
}

/// Helps providing the arguments in the [Subject.add] for
/// [ReportErrorBlocEvents.send] event
class _SendEventArgs {
  const _SendEventArgs(this.properties, this.message);

  final List<Property<dynamic>> properties;

  final String message;
}
