import 'package:mijngegevensapp/api/client.dart';
import 'package:mijngegevensapp/bloc/generic_bloc.dart';
import 'package:mijngegevensapp/model/history.dart';

class HistoryBloc extends GenericBloc<History> {
  HistoryBloc(final Client client) : super(client);
}
