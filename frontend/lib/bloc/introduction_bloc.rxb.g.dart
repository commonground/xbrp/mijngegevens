// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'introduction_bloc.dart';

/// Used as a contractor for the bloc, events and states classes
/// {@nodoc}
abstract class IntroductionBlocType extends RxBlocTypeBase {
  IntroductionBlocEvents get events;
  IntroductionBlocStates get states;
}

/// [$IntroductionBloc] extended by the [IntroductionBloc]
/// {@nodoc}
abstract class $IntroductionBloc extends RxBlocBase
    implements IntroductionBlocEvents, IntroductionBlocStates, IntroductionBlocType {
  final _compositeSubscription = CompositeSubscription();

  /// Тhe [Subject] where events sink to by calling [checkHasSeen]
  final _$checkHasSeenEvent = PublishSubject<void>();

  /// Тhe [Subject] where events sink to by calling [setHasSeen]
  final _$setHasSeenEvent = PublishSubject<void>();

  /// The state of [hasSeen] implemented in [_mapToHasSeenState]
  late final BehaviorSubject<bool> _hasSeenState = _mapToHasSeenState();

  @override
  void checkHasSeen() => _$checkHasSeenEvent.add(null);

  @override
  void setHasSeen() => _$setHasSeenEvent.add(null);

  @override
  BehaviorSubject<bool> get hasSeen => _hasSeenState;

  BehaviorSubject<bool> _mapToHasSeenState();

  @override
  IntroductionBlocEvents get events => this;

  @override
  IntroductionBlocStates get states => this;

  @override
  void dispose() {
    _$checkHasSeenEvent.close();
    _$setHasSeenEvent.close();
    _compositeSubscription.dispose();
    super.dispose();
  }
}
