import 'package:mijngegevensapp/api/client.dart';
import 'package:mijngegevensapp/bloc/generic_bloc.dart';
import 'package:mijngegevensapp/model/profile_picture.dart';

class ProfilePictureBloc extends GenericBloc<ProfilePicture> {
  ProfilePictureBloc(final Client client) : super(client);
}
