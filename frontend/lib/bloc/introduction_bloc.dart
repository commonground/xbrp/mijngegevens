import 'package:mijngegevensapp/api/preferences.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

part 'introduction_bloc.rxb.g.dart';

abstract class IntroductionBlocEvents {
  void checkHasSeen();
  void setHasSeen();
}

abstract class IntroductionBlocStates {
  BehaviorSubject<bool> get hasSeen;
}

@RxBloc()
class IntroductionBloc extends $IntroductionBloc {
  IntroductionBloc(this._preferences);

  final Preferences _preferences;

  final _subject = BehaviorSubject<bool>();

  @override
  void dispose() {
    _subject.close();
    super.dispose();
  }

  @override
  BehaviorSubject<bool> _mapToHasSeenState() {
    Rx.merge<bool>([
      _$checkHasSeenEvent.flatMap((final _) async* {
        yield _preferences.getHasSeenIntroduction();
      }),
      _$setHasSeenEvent.flatMap((final _) async* {
        _preferences.setHasSeenIntroduction();
        yield true;
      }),
    ]).listen(_subject.add);
    return _subject;
  }
}
