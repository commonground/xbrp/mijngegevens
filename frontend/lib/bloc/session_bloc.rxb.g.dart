// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'session_bloc.dart';

/// Used as a contractor for the bloc, events and states classes
/// {@nodoc}
abstract class SessionBlocType extends RxBlocTypeBase {
  SessionBlocEvents get events;
  SessionBlocStates get states;
}

/// [$SessionBloc] extended by the [SessionBloc]
/// {@nodoc}
abstract class $SessionBloc extends RxBlocBase implements SessionBlocEvents, SessionBlocStates, SessionBlocType {
  final _compositeSubscription = CompositeSubscription();

  /// Тhe [Subject] where events sink to by calling [checkIsLoggedIn]
  final _$checkIsLoggedInEvent = PublishSubject<void>();

  /// Тhe [Subject] where events sink to by calling [setIsLoggedIn]
  final _$setIsLoggedInEvent = PublishSubject<void>();

  /// The state of [isLoggedIn] implemented in [_mapToIsLoggedInState]
  late final BehaviorSubject<bool> _isLoggedInState = _mapToIsLoggedInState();

  @override
  void checkIsLoggedIn() => _$checkIsLoggedInEvent.add(null);

  @override
  void setIsLoggedIn() => _$setIsLoggedInEvent.add(null);

  @override
  BehaviorSubject<bool> get isLoggedIn => _isLoggedInState;

  BehaviorSubject<bool> _mapToIsLoggedInState();

  @override
  SessionBlocEvents get events => this;

  @override
  SessionBlocStates get states => this;

  @override
  void dispose() {
    _$checkIsLoggedInEvent.close();
    _$setIsLoggedInEvent.close();
    _compositeSubscription.dispose();
    super.dispose();
  }
}
