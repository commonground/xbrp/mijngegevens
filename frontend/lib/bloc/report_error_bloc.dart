import 'package:mijngegevensapp/api/client.dart';
import 'package:mijngegevensapp/model/property.dart';
import 'package:mijngegevensapp/model/report_error_request.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

part 'report_error_bloc.rxb.g.dart';

abstract class ReportErrorBlocEvents {
  void send(final List<Property> properties, final String message);
}

abstract class ReportErrorBlocStates {
  BehaviorSubject<Result<bool>> get sent;
}

@RxBloc()
class ReportErrorBloc extends $ReportErrorBloc {
  ReportErrorBloc(this.client);

  final Client client;

  final _subject = BehaviorSubject<Result<bool>>();

  @override
  void dispose() {
    super.dispose();
    _subject.close();
  }

  @override
  BehaviorSubject<Result<bool>> _mapToSentState() {
    _$sendEvent
        .flatMap<Result<bool>>((final event) async* {
          yield Result.loading();
          try {
            final response = await client.send<ReportErrorRequest, bool>(
              ReportErrorRequest(
                event.properties,
                event.message,
              ),
            );
            yield Result.success(response);
          } catch (e, s) {
            print(e);
            print(s);
            yield Result.error(Exception());
          }
        })
        .startWith(Result.success(false))
        .listen(_subject.add);
    return _subject;
  }
}
