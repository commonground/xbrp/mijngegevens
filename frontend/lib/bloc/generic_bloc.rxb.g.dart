// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'generic_bloc.dart';

/// Used as a contractor for the bloc, events and states classes
/// {@nodoc}
abstract class GenericBlocType extends RxBlocTypeBase {
  GenericBlocEvents get events;
  GenericBlocStates get states;
}

/// [$GenericBloc<T>] extended by the [GenericBloc<T>]
/// {@nodoc}
abstract class $GenericBloc<T> extends RxBlocBase implements GenericBlocEvents, GenericBlocStates, GenericBlocType {
  final _compositeSubscription = CompositeSubscription();

  /// Тhe [Subject] where events sink to by calling [load]
  final _$loadEvent = PublishSubject<void>();

  /// The state of [reloading] implemented in [_mapToReloadingState]
  late final BehaviorSubject<bool> _reloadingState = _mapToReloadingState();

  /// The state of [data] implemented in [_mapToDataState]
  late final BehaviorSubject<Result<T>> _dataState = _mapToDataState();

  @override
  void load() => _$loadEvent.add(null);

  @override
  BehaviorSubject<bool> get reloading => _reloadingState;

  @override
  BehaviorSubject<Result<T>> get data => _dataState;

  BehaviorSubject<bool> _mapToReloadingState();

  BehaviorSubject<Result<T>> _mapToDataState();

  @override
  GenericBlocEvents get events => this;

  @override
  GenericBlocStates get states => this;

  @override
  void dispose() {
    _$loadEvent.close();
    _compositeSubscription.dispose();
    super.dispose();
  }
}
