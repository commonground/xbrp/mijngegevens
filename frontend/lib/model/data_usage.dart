import 'package:mijngegevensapp/model/data_request.dart';

class DataUsage {
  DataUsage({
    required final this.requests,
  });

  final List<DataRequest> requests;
}
