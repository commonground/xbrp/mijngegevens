import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:mijngegevensapp/model/property.dart';

class HistoryEntry extends PropertyHolder {
  HistoryEntry(final List<Property> properties)
      : assert(
          properties.where((final p) => p.key == PropertyKey.eventID).length == 1,
          'there needs to be exactly one eventID property',
        ),
        assert(
          properties.where((final p) => p.key == PropertyKey.title).length == 1,
          'there needs to be exactly one title property',
        ),
        assert(
          properties.where((final p) => p.key == PropertyKey.category).length == 1,
          'there needs to be exactly one category property',
        ),
        assert(
          properties.where((final p) => p.key == PropertyKey.date).length == 1,
          'there needs to be exactly one date property',
        ),
        assert(
          properties.where((final p) => p.key == PropertyKey.municipality).length == 1,
          'there needs to be exactly one municipality property',
        ),
        super(properties);

  Property<String> get eventID => findProperty<String>(PropertyKey.eventID)!;
  Property<String> get title => findProperty<String>(PropertyKey.title)!;
  Property<HistoryCategory> get category => findProperty<HistoryCategory>(PropertyKey.category)!;
  Property<DateTime> get date => findProperty<DateTime>(PropertyKey.date)!;
  Property<String> get municipality => findProperty<String>(PropertyKey.municipality)!;
  Property<String>? get partner1 => findProperty<String>(PropertyKey.partner1);
  Property<String>? get partner2 => findProperty<String>(PropertyKey.partner2);
  Property<DateTime>? get weddingDate => findProperty<DateTime>(PropertyKey.weddingDate);

  List<Property> get _relevantProperties {
    List<Property?> properties = [];
    switch (category.value) {
      case HistoryCategory.relationship:
        properties = [
          partner1,
          partner2,
          weddingDate,
        ];
        break;
      default:
        break;
    }
    return properties.where((final p) => p != null).map<Property>((final p) => p!).toList();
  }

  bool get hasRelevantProperties => _relevantProperties.isNotEmpty;

  List<Property> get relevantProperties {
    final List<Property> properties = _relevantProperties;
    if (properties.isEmpty) {
      throw UnimplementedError('Unimplemented category: ${category.value}');
    }
    return properties;
  }
}

extension HistoryEntryHelper on HistoryEntry {
  IconData get icon {
    switch (category.value) {
      case HistoryCategory.info:
        return Icons.announcement;
      case HistoryCategory.relationship:
        return Icons.group;
      case HistoryCategory.residence:
        return Icons.house;
      case HistoryCategory.birth:
        return Icons.child_friendly;
      case HistoryCategory.bsn:
        return Icons.dialpad;
      case HistoryCategory.person:
        return Icons.person_add;
      case HistoryCategory.name:
        return Icons.face;
      case HistoryCategory.nationality:
        return Icons.flag;
      case HistoryCategory.parent:
        return Icons.escalator_warning;
      case HistoryCategory.sexDesignation:
        return MdiIcons.hammerWrench;
      default:
        throw Exception('Unknown category $category');
    }
  }
}

enum HistoryCategory {
  info,
  relationship,
  residence,
  birth,
  bsn,
  person,
  name,
  nationality,
  parent,
  sexDesignation,
}
