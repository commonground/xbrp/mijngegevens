import 'package:flutter/material.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/model/address.dart';
import 'package:mijngegevensapp/model/personal_data.dart';
import 'package:mijngegevensapp/utils/formats.dart';

enum PropertyKey {
  personalDataType,
  firstName,
  lastName,
  usedNameType,
  bsn,
  birthDate,
  birthPlace,
  birthCountry,
  dutchNationality,
  residentialAddress,
  municipality,
  parent,
  child,
  effectiveDateParentChildRelationship,
  marriedTo,
  marriageDate,
  marriagePlace,
  marriageCountry,
  relatives,
  eventID,
  title,
  category,
  date,
  partner1,
  partner2,
  weddingDate,
}

class Property<T> {
  Property(
    this.key,
    this.value, {
    final this.effectiveDate,
    final this.inclusionDate,
    final this.registeredBy,
  });

  final PropertyKey key;
  final T value;
  final DateTime? effectiveDate;
  final DateTime? inclusionDate;
  final String? registeredBy;

  String getLabel(final BuildContext context) {
    final localizations = AppLocalizations.of(context);
    switch (key) {
      case PropertyKey.firstName:
        return localizations.firstName;
      case PropertyKey.lastName:
        return localizations.lastName;
      case PropertyKey.usedNameType:
        return localizations.usedName;
      case PropertyKey.bsn:
        return localizations.bsn;
      case PropertyKey.birthDate:
        return localizations.birthDate;
      case PropertyKey.birthPlace:
        return localizations.birthPlace;
      case PropertyKey.birthCountry:
        return localizations.birthCountry;
      case PropertyKey.dutchNationality:
        return localizations.dutchNationality;
      case PropertyKey.residentialAddress:
        return localizations.residentialAddress;
      case PropertyKey.municipality:
        return localizations.municipality('');
      case PropertyKey.parent:
        return localizations.parent;
      case PropertyKey.child:
        return localizations.child;
      case PropertyKey.effectiveDateParentChildRelationship:
        return localizations.effectiveDateParentChildRelationship;
      case PropertyKey.marriedTo:
        return localizations.marriedTo;
      case PropertyKey.marriageDate:
        return localizations.marriageDate;
      case PropertyKey.marriagePlace:
        return localizations.marriagePlace;
      case PropertyKey.marriageCountry:
        return localizations.marriageCountry;
      case PropertyKey.partner1:
        return localizations.partner1;
      case PropertyKey.partner2:
        return localizations.partner2;
      case PropertyKey.weddingDate:
        return localizations.weddingDate;
      case PropertyKey.eventID:
        return localizations.eventID;
      default:
        throw UnimplementedError('Unimplemented label for key: $key');
    }
  }

  String getDisplayValue(final BuildContext context) {
    if (T == String) {
      return value as String;
    }
    if (T == UsedNameType) {
      if ((value as UsedNameType) == UsedNameType.ownName) {
        return AppLocalizations.of(context).ownName;
      } else {
        return 'TODO';
      }
    }
    if (T == DateTime) {
      return smartFormat(value as DateTime);
    }
    if (T == bool) {
      if (value as bool) {
        return AppLocalizations.of(context).yes;
      } else {
        return AppLocalizations.of(context).no;
      }
    }
    if (T == Address) {
      return '${(value as Address).streetName} ${(value as Address).houseNumber}\n${(value as Address).postalCode}';
    }
    throw UnimplementedError('Unknown method to get display value for: ${T.runtimeType}');
  }
}

abstract class PropertyHolder {
  PropertyHolder(this.properties);

  final List<Property> properties;

  Property<T>? findProperty<T>(final PropertyKey key) {
    for (final property in properties) {
      if (property.key == key) {
        return property as Property<T>;
      }
    }
  }
}
