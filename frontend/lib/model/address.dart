class Address {
  Address({
    required final this.streetName,
    required final this.houseNumber,
    required final this.postalCode,
  });

  final String streetName;

  final String houseNumber;

  // TODO: How does this system actually work?
  final String postalCode;
}
