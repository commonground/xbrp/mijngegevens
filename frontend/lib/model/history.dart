import 'package:mijngegevensapp/model/history_entry.dart';

class History {
  History({
    required final this.entries,
  });

  final List<HistoryEntry> entries;
}
