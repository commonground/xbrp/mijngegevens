import 'package:mijngegevensapp/model/address.dart';
import 'package:mijngegevensapp/model/property.dart';

class PersonalData extends PropertyHolder {
  PersonalData(final List<Property> properties)
      : assert(
          properties.where((final p) => p.key == PropertyKey.personalDataType).length == 1,
          'there needs to be exactly one personalDataType property',
        ),
        assert(
          properties.where((final p) => p.key == PropertyKey.firstName).length == 1,
          'there needs to be exactly one firstName property',
        ),
        assert(
          properties.where((final p) => p.key == PropertyKey.lastName).length == 1,
          'there needs to be exactly one lastName property',
        ),
        assert(
          properties.where((final p) => p.key == PropertyKey.municipality).length == 1,
          'there needs to be exactly one municipality property',
        ),
        super(properties);

  Property<PersonalDataType> get type => findProperty<PersonalDataType>(PropertyKey.personalDataType)!;
  Property<String> get firstName => findProperty<String>(PropertyKey.firstName)!;
  Property<String> get lastName => findProperty<String>(PropertyKey.lastName)!;
  Property<UsedNameType>? get usedNameType => findProperty<UsedNameType>(PropertyKey.usedNameType);
  Property<String>? get bsn => findProperty<String>(PropertyKey.bsn);
  Property<DateTime>? get birthDate => findProperty<DateTime>(PropertyKey.birthDate);
  Property<String>? get birthPlace => findProperty<String>(PropertyKey.birthPlace);
  Property<String>? get birthCountry => findProperty<String>(PropertyKey.birthCountry);
  Property<bool>? get dutchNationality => findProperty<bool>(PropertyKey.dutchNationality);
  Property<Address>? get residentialAddress => findProperty<Address>(PropertyKey.residentialAddress);
  Property<String> get municipality => findProperty<String>(PropertyKey.municipality)!;
  Property<String>? get parent => findProperty<String>(PropertyKey.parent);
  Property<String>? get child => findProperty<String>(PropertyKey.child);
  Property<DateTime>? get effectiveDateParentChildRelationship =>
      findProperty<DateTime>(PropertyKey.effectiveDateParentChildRelationship);
  Property<String>? get marriedTo => findProperty<String>(PropertyKey.marriedTo);
  Property<DateTime>? get marriageDate => findProperty<DateTime>(PropertyKey.marriageDate);
  Property<String>? get marriagePlace => findProperty<String>(PropertyKey.marriagePlace);
  Property<String>? get marriageCountry => findProperty<String>(PropertyKey.marriageCountry);
  Property<List<PersonalData>>? get relatives => findProperty<List<PersonalData>>(PropertyKey.relatives);

  List<Property> get basicDataProperties => [
        firstName,
        lastName,
        usedNameType,
        bsn,
        birthDate,
        birthPlace,
        birthCountry,
        dutchNationality,
      ].where((final property) => property != null).map((final property) => property!).toList();

  List<Property> get residentialAddressProperties => [
        residentialAddress,
        municipality,
      ].where((final property) => property != null).map((final property) => property!).toList();

  List<Property> get parentProperties => [
        parent,
        effectiveDateParentChildRelationship,
      ].where((final property) => property != null).map((final property) => property!).toList();

  List<Property> get childProperties => [
        child,
        effectiveDateParentChildRelationship,
      ].where((final property) => property != null).map((final property) => property!).toList();

  List<Property> get relationshipProperties => [
        marriedTo,
        marriageDate,
        marriagePlace,
        marriageCountry,
      ].where((final property) => property != null).map((final property) => property!).toList();
}

extension PersonalDataHelper on PersonalData {
  String get fullName => '${firstName.value} ${lastName.value}';

  List<PersonalData> get parents =>
      relatives == null ? [] : relatives!.value.where((final r) => r.type.value == PersonalDataType.parent).toList();

  PersonalData? get relationship {
    if (relatives == null) {
      return null;
    }
    final relationships = relatives!.value.where((final r) => r.type.value == PersonalDataType.relationship);
    if (relationships.isEmpty) {
      return null;
    }
    return relationships.single;
  }

  List<PersonalData> get children =>
      relatives == null ? [] : relatives!.value.where((final r) => r.type.value == PersonalDataType.child).toList();
}

enum PersonalDataType {
  self,
  parent,
  relationship,
  child,
}

enum UsedNameType {
  ownName,
}
