import 'package:mijngegevensapp/model/property.dart';

class ReportErrorRequest {
  ReportErrorRequest(
    this.properties,
    this.message,
  );

  final List<Property> properties;
  final String message;
}
