import 'dart:typed_data';

class ProfilePicture {
  ProfilePicture(this.data);

  final Uint8List data;
}
