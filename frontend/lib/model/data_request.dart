class DataRequest {
  DataRequest({
    required final this.organization,
    required final this.date,
    required final this.reasonOfUse,
    required final this.reason,
    required final this.accessedInformation,
  });

  final String organization;
  final DateTime date;
  final String reasonOfUse;
  final String reason;
  final Set<InformationType> accessedInformation;
}

enum InformationType {
  basicInformation,
  residentialAddress,
}
