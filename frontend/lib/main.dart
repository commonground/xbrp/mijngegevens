import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:mijngegevensapp/api/client.dart';
import 'package:mijngegevensapp/api/preferences.dart';
import 'package:mijngegevensapp/bloc/data_usage_bloc.dart';
import 'package:mijngegevensapp/bloc/history_bloc.dart';
import 'package:mijngegevensapp/bloc/introduction_bloc.dart';
import 'package:mijngegevensapp/bloc/personal_data_bloc.dart';
import 'package:mijngegevensapp/bloc/profile_picture_bloc.dart';
import 'package:mijngegevensapp/bloc/session_bloc.dart';
import 'package:mijngegevensapp/l10n/localizations.dart';
import 'package:mijngegevensapp/pages/home_page.dart';
import 'package:mijngegevensapp/utils/theme.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    MyApp(
      await SharedPreferences.getInstance(),
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp(
    this.sharedPreferences, {
    final Key? key,
  }) : super(key: key);

  final SharedPreferences sharedPreferences;

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Preferences preferences;
  late Client client;
  late IntroductionBloc introductionBloc;
  late SessionBloc sessionBloc;
  late PersonalDataBloc personalDataBloc;
  late DataUsageBloc dataUsageBloc;
  late ProfilePictureBloc profilePictureBloc;
  late HistoryBloc historyBloc;

  @override
  void initState() {
    super.initState();
    preferences = Preferences(widget.sharedPreferences);
    client = Client();
    introductionBloc = IntroductionBloc(preferences);
    sessionBloc = SessionBloc(preferences);
    personalDataBloc = PersonalDataBloc(client);
    dataUsageBloc = DataUsageBloc(client);
    profilePictureBloc = ProfilePictureBloc(client);
    historyBloc = HistoryBloc(client);
  }

  @override
  Widget build(final BuildContext context) => RxMultiBlocProvider(
        providers: [
          RxBlocProvider<IntroductionBloc>(
            create: (final _) => introductionBloc,
          ),
          RxBlocProvider<SessionBloc>(
            create: (final _) => sessionBloc,
          ),
          RxBlocProvider<PersonalDataBloc>(
            create: (final _) => personalDataBloc,
          ),
          RxBlocProvider<DataUsageBloc>(
            create: (final _) => dataUsageBloc,
          ),
          RxBlocProvider<ProfilePictureBloc>(
            create: (final _) => profilePictureBloc,
          ),
          RxBlocProvider<HistoryBloc>(
            create: (final _) => historyBloc,
          ),
        ],
        child: MultiProvider(
          providers: [
            Provider<Client>(
              create: (final _) => client,
            ),
          ],
          child: MaterialApp(
            onGenerateTitle: (final context) => AppLocalizations.of(context).title,
            theme: mijngegevensAppTheme(context),
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            scrollBehavior: CustomScrollBehavior(),
            home: const HomePage(),
          ),
        ),
      );
}

class CustomScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => PointerDeviceKind.values.toSet();
}
