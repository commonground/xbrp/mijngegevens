# helm/mijngegevens

## Values

Configuration for the helm chart can be provided through a values.yaml file.

| Variable | Description |
| -------- | ----------- |
| `env` | Name of the environment for which the chart is deployed. This value is used in domain names. |

## Components

Notes about specific components
